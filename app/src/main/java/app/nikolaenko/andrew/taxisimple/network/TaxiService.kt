package app.nikolaenko.andrew.taxisimple.network

import app.nikolaenko.andrew.taxisimple.model.ChatResponse
import app.nikolaenko.andrew.taxisimple.model.*
import io.reactivex.Observable
import okhttp3.MultipartBody
import retrofit2.http.*

interface TaxiService {

    @GET("/api/performer/getAutoOrder")
    fun getAutoOrder(@Query("token") token: String): Observable<OrderTypeModel>

    @FormUrlEncoded
    @POST("api/performer/setAutoOrder")
    fun setAutoOrder(@Field("token")token: String, @Query("option") option: Int): Observable<DataResponse>

    @GET("/api/performer/getFinance")
    fun getFinance(@Query("token") token: String): Observable<FinanceModel>

    @GET("/api/performer/getInfo")
    fun getDriverInfo(@Query("token") token: String): Observable<DriverModel>

    @GET("/api/performer/getReports")
    fun getReports(@Query("token") token: String): Observable<ReportsResponse>

    @GET("/api/performer/getInfoReport")
    fun getReportInfo(@Query("token")token: String, @Query("report_id") reportId: Int): Observable<ReportInfoResponse>

    @FormUrlEncoded
    @POST("/api/performer/registerReport")
    fun createReport(@Field("token") token: String, @Field("topic") topic: String,
                     @Field("text") text: String): Observable<DataResponse>

    @Multipart
    @POST("/api/performer/registerReport")
    fun createReportWithImage(@Part body: MultipartBody.Part, @Part body2: MultipartBody.Part, @Part body3: MultipartBody.Part,
                              @Part body4: MultipartBody.Part): Observable<DataResponse>

    @FormUrlEncoded
    @POST("/api/performer/sendMessageToChat")
    fun sendMessage(@Field("token") token: String, @Field("performer_id") userID: Int,
                    @Field("message") message: String): Observable<ChatResponse>

    @GET("/api/performer/getMessagesFromChat")
    fun getMessageList(@Query("token")token: String, @Query("limit") limit: Int, @Query("offset") offset: Int,
                       @Query("order_by_date") orderByDate: String): Observable<MessagesResponse>

    @FormUrlEncoded
    @POST("/api/performer/answerReport")
    fun answerToReport(@Field("token") token: String, @Field("reports_id") reportsId: Int,
                       @Field("answer") answer: String): Observable<DataResponse>

    @Multipart
    @POST("/api/performer/answerReport")
    fun answerToReportWithImage(@Part body: MultipartBody.Part, @Part body2: MultipartBody.Part,
                                @Part body3: MultipartBody.Part, @Part body4: MultipartBody.Part): Observable<DataResponse>

    @GET("/api/performer/getLastOrders")
    fun getLastOrders(@Query("token") token: String) : Observable<OrdersResponse>

    @GET("/api/performer/getTransactions")
    fun getTransactions(@Query("token") token: String) : Observable<TransactionsResponse>

    @GET("/api/performer/setFirebaseToken")
    fun sendFirebaseToken(@Query("token") token: String, @Query("firebase_token") firebaseToken: String): Observable<DataResponse>

    @GET("/api/performer/getOrdersOnMap")
    fun getOrdersOnMap(@Query("token") token: String): Observable<OrderResponse>

    @GET("/api/performer/getOrderInfo")
//    fun getOrderInfo(@Query("token") token: String, @Query("order_id") orderId: Int): Observable<OrderInfoResponse>
    fun getOrderInfo(@Query("token") token: String): Observable<OrderInfoResponse>

    @FormUrlEncoded
    @POST("/api/performer/setPerformerLocation")
    fun updateLocation(@Field("token") token: String, @Field("latitude") latitude: String,
                       @Query("Field") longitude: String): Observable<DataResponse>

    @FormUrlEncoded
    @POST("/api/performer/confirmOrder")
    fun confirmOrder(@Field("token") token: String, @Field("order_id") orderId: Int): Observable<DataResponse>

    @FormUrlEncoded
    @POST("/api/performer/setPerformerOrderStatus")
    fun setPerformerOrderStatus(@Field("token") token: String, @Field("order_id") orderId: Int,
                                @Field("status") status: Int, @Field("latitude") latitude: String,
                                @Field("longitude") longitude: String): Observable<DataResponse>

    @FormUrlEncoded
    @POST("/api/performer/finishOrder")
    fun finishOrder(@Field("token") token: String, @Field("order_id") orderId: Int): Observable<DataResponse>

}
