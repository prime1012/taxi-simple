package app.nikolaenko.andrew.taxisimple.model

import com.google.gson.annotations.SerializedName

class DriverModel {

    @SerializedName("id")
    var id: Int? = null

    @SerializedName("phoneNumber")
    var phoneNumber: String? = null

    @SerializedName("type")
    var type: Int? = null

    @SerializedName("surname")
    var surname: String? = null

    @SerializedName("name")
    var name: String? = null

    @SerializedName("middleName")
    var middleName: String? = null

    @SerializedName("dateOfBirth")
    var dateOfBirth: String? = null

    @SerializedName("driverLicense")
    var driverLicense: DriverLicenseModel? = null

    @SerializedName("bank_card")
    var bankСard: String? = null

    @SerializedName("email")
    var email: String? = null

    @SerializedName("photo")
    var photo: String? = null

    @SerializedName("driver_license_photo")
    var driverLicensePhoto: String? = null

    @SerializedName("rating")
    var rating: String? = null

    @SerializedName("balance")
    var balance: Int? = null

    @SerializedName("status")
    var status: Int? = null

    @SerializedName("hasActiveOrder")
    var hasActiveOrder: Boolean? = false

    @SerializedName("need_registration")
    var needRegistration: Boolean? = false

    @SerializedName("validate")
    var validate: Boolean? = false

    @SerializedName("id_cars")
    var idCars: Int? = null

    @SerializedName("mark")
    var mark: String? = null

    @SerializedName("model")
    var model: String? = null

    @SerializedName("color_car")
    var colorCar: String? = null

    @SerializedName("state_number")
    var stateNumber: String? = null

    @SerializedName("vin_code")
    var vinCode: String? = null

    @SerializedName("mileage")
    var mileage: Int? = null

    @SerializedName("osago_date_issue")
    var osagoDateIssue: String? = null

    @SerializedName("osago_end_date")
    var osagoEndDate: String? = null

    @SerializedName("diagnostic_card_date_issue")
    var diagnosticCardDateIssue: String? = null

    @SerializedName("diagnostic_card_end_date")
    var diagnosticCardEndDate: String? = null

    @SerializedName("year_of_issue")
    var yearOfIssue: String? = null

    @SerializedName("information_dtp")
    var informationDtp: String? = null

    @SerializedName("date_of_maintenance")
    var dateOfMaintenance: String? = null

    @SerializedName("callsign")
    var callsign: String? = null
}