package app.nikolaenko.andrew.taxisimple.model

import com.google.gson.annotations.SerializedName

class ChatResponse {

    @SerializedName("result")
    var resultChat: String? = null
}