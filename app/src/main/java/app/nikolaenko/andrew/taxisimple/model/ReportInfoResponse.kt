package app.nikolaenko.andrew.taxisimple.model

import com.google.gson.annotations.SerializedName

class ReportInfoResponse {

    @SerializedName("report")
    var report: ReportObject? = null
}