package app.nikolaenko.andrew.taxisimple.fragment

import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import app.nikolaenko.andrew.taxisimple.R
import app.nikolaenko.andrew.taxisimple.adapter.ImagesAdapter
import app.nikolaenko.andrew.taxisimple.model.DriverModel
import app.nikolaenko.andrew.taxisimple.model.Image
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.about_auto_fragment.*
import org.jetbrains.anko.support.v4.toast


class AboutAutoFragment : BaseFragment() {

    private lateinit var imagesAdapter: ImagesAdapter

    private var infoDisposable: Disposable? = null

    override fun onStart() {
        super.onStart()
        changeFont()

        getDriverInfo()

        val list = ArrayList<Image>()
        list.clear()
        list.add(0, Image(R.drawable.rectangle))
        list.add(1, Image(R.drawable.rectangle))
        list.add(2, Image(R.drawable.rectangle))
        list.add(3, Image(R.drawable.rectangle))
        imagesAdapter.setList(list)

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.about_auto_fragment, container, false)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        imagesAdapter = ImagesAdapter()
        rvImages.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        rvImages.itemAnimator = DefaultItemAnimator()
        rvImages.adapter = imagesAdapter
        rvImages.isNestedScrollingEnabled = false

        rvImages.addOnItemTouchListener(object : RecyclerView.OnItemTouchListener {
            override fun onInterceptTouchEvent(rv: RecyclerView, e: MotionEvent): Boolean {
                val action = e.action
                when (action) {
                    MotionEvent.ACTION_MOVE -> rv.parent.requestDisallowInterceptTouchEvent(true)
                }
                return false
            }

            override fun onTouchEvent(rv: RecyclerView, e: MotionEvent) {

            }

            override fun onRequestDisallowInterceptTouchEvent(disallowIntercept: Boolean) {

            }
        })
    }

    private fun changeFont(){
        changeFontInTextView(text2)
        changeFontInTextView(vinText)
        changeFontInTextView(colorValue)
        changeFontInTextView(yearText)


        changeFontInTextViewBold(autoModelText)
        changeFontInTextViewBold(vinValue)
        changeFontInTextViewBold(yearValue)

    }

    private fun getDriverInfo(){

        showProgress(true)
        infoDisposable = taxiService.value.getDriverInfo(loginService.value.accessToken)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ t: DriverModel ->

                    autoModelText.text = t.mark + " " + t.model + " - " + t.stateNumber

                    t.vinCode?.let { vinValue.text = it }
                    t.colorCar?.let { colorValue.text = it }
//                    yearValue.text = "2018"

                    if (t.yearOfIssue != null){
                        val sb = StringBuilder(t.yearOfIssue)
                        sb.delete(4, t.yearOfIssue!!.length -1)
                        sb.deleteCharAt(4)
                        val result2 = sb.toString()

                        yearValue.text = result2
                    }

                    when (t.colorCar){
                        "морская волна" -> { colorCard.setCardBackgroundColor(ContextCompat.getColor(context!!, R.color.sea_wave_color))}
                        "чёрный" -> { colorCard.setCardBackgroundColor(ContextCompat.getColor(context!!, R.color.black))}
                        "голубой" -> { colorCard.setCardBackgroundColor(ContextCompat.getColor(context!!, R.color.blue_color))}
                        "коричневый" -> { colorCard.setCardBackgroundColor(ContextCompat.getColor(context!!, R.color.brown_color))}
                        "фуксия" -> { colorCard.setCardBackgroundColor(ContextCompat.getColor(context!!, R.color.fixia_color))}
                        "серый" -> { colorCard.setCardBackgroundColor(ContextCompat.getColor(context!!, R.color.gray_color))}
                        "зелёный" -> { colorCard.setCardBackgroundColor(ContextCompat.getColor(context!!, R.color.green_color))}
                        "лаймовый цвет" -> { colorCard.setCardBackgroundColor(ContextCompat.getColor(context!!, R.color.lime_color))}
                        "тёмно-бордовый" -> { colorCard.setCardBackgroundColor(ContextCompat.getColor(context!!, R.color.dark_red_color))}
                        "тёмно-синий" -> { colorCard.setCardBackgroundColor(ContextCompat.getColor(context!!, R.color.dark_blue_color))}
                        "оливковый" -> { colorCard.setCardBackgroundColor(ContextCompat.getColor(context!!, R.color.olive_color))}
                        "оранжевый" -> { colorCard.setCardBackgroundColor(ContextCompat.getColor(context!!, R.color.orange_color))}
                        "розовый" -> { colorCard.setCardBackgroundColor(ContextCompat.getColor(context!!, R.color.pink_color))}
                        "фиолетовый" -> { colorCard.setCardBackgroundColor(ContextCompat.getColor(context!!, R.color.violet_color))}
                        "красный" -> { colorCard.setCardBackgroundColor(ContextCompat.getColor(context!!, R.color.red))}
                        "серебряный" -> { colorCard.setCardBackgroundColor(ContextCompat.getColor(context!!, R.color.silver_color))}
                        "сине-зелёный" -> { colorCard.setCardBackgroundColor(ContextCompat.getColor(context!!, R.color.blue_green_color))}
                        "белый" -> { colorCard.setCardBackgroundColor(ContextCompat.getColor(context!!, R.color.white))}
                        "жёлтый" -> { colorCard.setCardBackgroundColor(ContextCompat.getColor(context!!, R.color.yellow_color))}
                    }

                    showProgress(false)

                }, { e ->
                    e.printStackTrace()
                    toast(e.localizedMessage)
                    showProgress(false)
                })
    }


    override fun onStop() {
        super.onStop()
        infoDisposable?.dispose()
    }
}
