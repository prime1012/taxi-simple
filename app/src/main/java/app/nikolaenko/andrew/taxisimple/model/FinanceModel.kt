package app.nikolaenko.andrew.taxisimple.model

import com.google.gson.annotations.SerializedName

class FinanceModel {

    @SerializedName("currentMonth")
    var currentMonth: String? = null

    @SerializedName("currentWeek")
    var currentWeek: String? = null

//    @SerializedName("finance")
//    var finance: FinanceModel
}