package app.nikolaenko.andrew.taxisimple.adapter


import android.graphics.Typeface
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import app.nikolaenko.andrew.taxisimple.R
import app.nikolaenko.andrew.taxisimple.model.Image
import app.nikolaenko.andrew.taxisimple.model.RideModel
import com.bumptech.glide.Glide
import org.jetbrains.anko.find

class RidesAdapter : RecyclerView.Adapter<RidesAdapter.MyViewHolder>() {

    private var rideList: List<RideModel> = listOf()

    fun setList(list: List<RideModel>) {
        rideList = list
        notifyDataSetChanged()
    }

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var name: TextView = view.find(R.id.name)
        var price: TextView = view.find(R.id.priceValue)
        var priceCurrency: TextView = view.find(R.id.priceCurrency)
        var textDateTime: TextView = view.find(R.id.textDateTime)
        var starCount: TextView = view.find(R.id.starCount)
        var textFrom: TextView = view.find(R.id.textFrom)
        var textTo: TextView = view.find(R.id.textTo)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.travel_item, parent, false)

        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val rideItem = rideList[position]

        val type = Typeface.createFromAsset(holder.itemView.context?.assets, "font/opensans_regular.ttf")
        val typeBold = Typeface.createFromAsset(holder.itemView.context?.assets, "font/opensans_bold.ttf")

        holder.name.text = rideItem.clientFullName
        holder.price.text = rideItem.amount.toString()
        holder.priceCurrency.text = "руб"
        holder.textDateTime.text = rideItem.orderDate
        holder.starCount.text = "5"
        holder.textFrom.text = rideItem.originText
        holder.textTo.text = rideItem.destinationText

        holder.name.typeface = type
        holder.price.typeface = typeBold
        holder.priceCurrency.typeface = type
        holder.starCount.typeface = type
        holder.textFrom.typeface = type
        holder.textTo.typeface = type
        holder.textDateTime.typeface = typeBold
    }

    override fun getItemCount(): Int {
        return rideList.size
    }
}
