package app.nikolaenko.andrew.taxisimple.network


import android.content.Context
import app.nikolaenko.andrew.taxisimple.R
import com.pixplicity.easyprefs.library.Prefs
import retrofit2.Call

class LoginService(private val context: Context, private val yammyPublicService: TaxiPublicService) {

    private val yammyService: TaxiService

    val accessToken: String
        get() = Prefs.getString(ACCESS_TOKEN_KEY, "")

    init {
        val resources = context.resources
        val baseUrl = resources.getString(R.string.api_base_url)
//
        yammyService = RetrofitFactory.getAuthorized(baseUrl, this
                , context
        ).create(TaxiService::class.java)
    }

//    fun login(username: String, password: String, phone: String, callback: YammyLoginCallback) {
//        val loginCall = yammyPublicService.login(LOGIN_TYPE_NATIVE, username, phone, password, null, null)
//
//        processResponseAsync(loginCall, callback)
//    }

    fun logout() {
        Prefs.putString(ACCESS_TOKEN_KEY, null)
        Prefs.clear()
    }

    fun isLoggedIn(): Boolean = Prefs.getString(ACCESS_TOKEN_KEY, "") != ""

    companion object {
        val ACCESS_TOKEN_KEY = "access_token"
    }
}
