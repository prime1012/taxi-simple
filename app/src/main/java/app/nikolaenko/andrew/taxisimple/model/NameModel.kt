package app.nikolaenko.andrew.taxisimple.model

import com.google.gson.annotations.SerializedName

class NameModel {

    @SerializedName("name")
    var name: String? = null

    @SerializedName("surname")
    var surname: String? = null

    @SerializedName("middleName")
    var middleName: String? = null
}