package app.nikolaenko.andrew.taxisimple.adapter


import android.graphics.Typeface
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import app.nikolaenko.andrew.taxisimple.R
import app.nikolaenko.andrew.taxisimple.model.Report
import org.jetbrains.anko.find
import org.jetbrains.anko.textColor
import java.text.SimpleDateFormat

class ReportsAdapter(private val callback: ((id: Int) -> Unit)? = null) : RecyclerView.Adapter<ReportsAdapter.MyViewHolder>() {

    private var newsList: List<Report> = listOf()

    fun setList(list: List<Report>) {
        newsList = list
        notifyDataSetChanged()
    }

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var textStatus: TextView = view.find(R.id.textStatus)
        var textTime: TextView = view.find(R.id.textTime)
        var title: TextView = view.find(R.id.title)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.support_themes_item, parent, false)

        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val item = newsList[position]

        val type = Typeface.createFromAsset(holder.itemView.context?.assets, "font/opensans_regular.ttf")
        val typeBold = Typeface.createFromAsset(holder.itemView.context?.assets, "font/opensans_bold.ttf")

        if (item.checked == 0){
            holder.textStatus.text = "Обращение открыто"
            holder.textStatus.textColor = ContextCompat.getColor(holder.itemView.context, R.color.main_green)

            holder.title.text = item.topic
            holder.title.textColor = ContextCompat.getColor(holder.itemView.context, R.color.black)
        } else {
            holder.textStatus.text = "Обращение закрыто"
            holder.textStatus.textColor = ContextCompat.getColor(holder.itemView.context, R.color.main_orange)

            holder.title.text = item.topic
            holder.title.textColor = ContextCompat.getColor(holder.itemView.context, R.color.main_gray)
        }

        val formatter = SimpleDateFormat("dd.MM.yyyy")
        val date = formatter.format(item.timestamp!! * 1000)
        holder.textTime.text = "$date год"

        holder.textStatus.typeface = type
        holder.title.typeface = typeBold
        holder.textTime.typeface = type

        holder.itemView.setOnClickListener {
            callback?.invoke(item.id!!)
        }
    }

    override fun getItemCount(): Int {
        return newsList.size
    }
}
