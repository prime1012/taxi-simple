package app.nikolaenko.andrew.taxisimple.fragment

import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import app.nikolaenko.andrew.taxisimple.R
import app.nikolaenko.andrew.taxisimple.model.DriverModel
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.pixplicity.easyprefs.library.Prefs
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.account_settings_fragment.*
import org.jetbrains.anko.support.v4.toast


class AccountSettingsFragment : BaseFragment() {

    private var infoDisposable: Disposable? = null

    override fun onStart() {
        super.onStart()
        changeFont()

        getDriverInfo()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.account_settings_fragment, container, false)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        rightBtn.setOnClickListener {
            showProgress(true)
            val handler = Handler()
            handler.postDelayed({
                showProgress(false)
                mainActivity.hardReplaceFragment(DriverMainFragment())
            }, 2000)
        }

        leftBtn.setOnClickListener {
            mainActivity.hardReplaceFragment(DriverMainFragment())
        }

        exitBtn.setOnClickListener {
            mainActivity.finish()
        }
    }

    private fun getDriverInfo(){

//        val info = Prefs.getString("driver_info", null)
//        val gson = Gson()
//        val mainResponse = gson.fromJson(info, DriverModel::class.java)

        showProgress(true)
        infoDisposable = taxiService.value.getDriverInfo(loginService.value.accessToken)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ t: DriverModel ->

                    userNameTv.text = "${t.name} ${t.middleName} ${t.surname}"

                    if (t.photo != null) {

                        if (t.photo!!.isNotEmpty()) {
                            Glide.with(context!!).load(t.photo + "/performer?token=${loginService.value.accessToken}").into(userImg)
                        } else {
                            Glide.with(context!!).load(R.drawable.avatar_placeholder).into(userImg)
                        }
                    }

                    t.rating?.let { starCount.text = it }

                    val gson = Gson()
                    val json = gson.toJson(t)
                    Prefs.putString("driver_info", json)

                    autoTv.text = t.mark + " " + t.model + " - " + t.stateNumber
                    radiusValue.text = "2км"
                    voiceValue.text = "Мужской"
                    languageValue.text = "Русский"
                    themeValue.text = "Дневная"

                    showProgress(false)

                }, { e ->
                    e.printStackTrace()
                    toast(e.localizedMessage)
                    showProgress(false)
                })

    }

    private fun changeFont(){
        changeFontInTextView(starCount)
        changeFontInTextView(textTitle)
        changeFontInTextView(autoTv)
        changeFontInTextView(navigationText)
        changeFontInTextView(radiusText)
        changeFontInTextView(radiusValue)
        changeFontInTextView(voiceText)
        changeFontInTextView(voiceValue)
        changeFontInTextView(languageText)
        changeFontInTextView(languageValue)
        changeFontInTextView(themeText)
        changeFontInTextView(themeValue)

        changeFontInTextViewBold(userNameTv)
        changeFontInTextViewBold(yandexIcon)
        changeFontInTextViewBold(yandexText)
        changeFontInTextViewBold(textExit)
    }

    override fun onStop() {
        super.onStop()
        infoDisposable?.dispose()
    }
}
