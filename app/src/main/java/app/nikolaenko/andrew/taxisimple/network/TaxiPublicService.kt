package app.nikolaenko.andrew.taxisimple.network

import app.nikolaenko.andrew.taxisimple.model.LoginResponse
import io.reactivex.Observable
import retrofit2.Call
import retrofit2.http.*
import kotlin.collections.HashMap
import okhttp3.RequestBody
import okhttp3.MultipartBody
import retrofit2.http.POST
import retrofit2.http.Multipart



interface TaxiPublicService {

    @FormUrlEncoded
    @POST("/api/authPerformerByDriverLicense")
    fun signInObservable(@Field("phone_number") phoneNumber: String, @Field("driverLicenseSeriesAndNumber") driverLicenseSeriesAndNumber: String) :Observable<LoginResponse>
}

