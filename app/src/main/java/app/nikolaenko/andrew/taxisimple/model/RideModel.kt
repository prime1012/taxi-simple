package app.nikolaenko.andrew.taxisimple.model

import com.google.gson.annotations.SerializedName

class RideModel {

    @SerializedName("id")
    var id: Int? = null

    @SerializedName("origin_text")
    var originText: String? = null

    @SerializedName("destination_text")
    var destinationText: String? = null

    @SerializedName("order_date")
    var orderDate: String? = null

    @SerializedName("client_full_name")
    var clientFullName: String? = null

    @SerializedName("amount")
    var amount: Int? = null
}