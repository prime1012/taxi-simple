package app.nikolaenko.andrew.taxisimple.model

import com.google.gson.annotations.SerializedName

class OrderTypeModel {

    @SerializedName("status")
    var status: String? = null

    @SerializedName("option")
    var option: Int? = null
}