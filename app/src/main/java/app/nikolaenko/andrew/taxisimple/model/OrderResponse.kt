package app.nikolaenko.andrew.taxisimple.model

import com.google.gson.annotations.SerializedName

class OrderResponse {

    @SerializedName("status")
    var status: String? = null

    @SerializedName("orders_on_map")
    var ordersOnMap: List<OrderModel>? = null
}