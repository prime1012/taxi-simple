package app.nikolaenko.andrew.taxisimple.model

import com.google.gson.annotations.SerializedName

class AnswerModel {

    @SerializedName("id")
    var id: Int? = null

    @SerializedName("report_id")
    var reportId: Int? = null

    @SerializedName("answer")
    var answer: String? = null

    @SerializedName("path_file")
    var pathFile: String? = null

    @SerializedName("created_at")
    var createdAt: String? = null

    @SerializedName("updated_at")
    var updatedAt: String? = null

    @SerializedName("account_type")
    var accountType: Int? = null

    @SerializedName("user_info")
    var userInfo: UserModel? = null

}