package app.nikolaenko.andrew.taxisimple.model

import com.google.gson.annotations.SerializedName

class DataResponse {

    @SerializedName("status")
    var status: String? = null
}