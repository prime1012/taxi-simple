package app.nikolaenko.andrew.taxisimple.fragment

import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import app.nikolaenko.andrew.taxisimple.R
import app.nikolaenko.andrew.taxisimple.adapter.RidesAdapter
import app.nikolaenko.andrew.taxisimple.model.OrdersResponse
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.rides_fragment.*
import org.jetbrains.anko.support.v4.toast


class RidesFragment : BaseFragment() {

    private lateinit var ridesAdapter: RidesAdapter
    private var ridesDisposable: Disposable? = null

    override fun onStart() {
        super.onStart()

        getLastRides()

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.rides_fragment, container, false)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ridesAdapter = RidesAdapter()
        rvRides.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        rvRides.itemAnimator = DefaultItemAnimator()
        rvRides.adapter = ridesAdapter
        rvRides.isNestedScrollingEnabled = false
    }

    private fun getLastRides(){
        ridesDisposable = taxiService.value.getLastOrders(loginService.value.accessToken)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ t: OrdersResponse ->

                    if (t.status == "success"){
                        if (t.completedOrders != null){
                            ridesAdapter.setList(t.completedOrders!!)
                        }
                    }

                }, { e ->
                    e.printStackTrace()
                    toast(e.localizedMessage)
                    showProgress(false)
                })
    }

    override fun onStop() {
        super.onStop()
        ridesDisposable?.dispose()
    }
}
