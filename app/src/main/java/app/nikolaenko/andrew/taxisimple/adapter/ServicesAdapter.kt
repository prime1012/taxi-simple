package app.nikolaenko.andrew.taxisimple.adapter


import android.graphics.Typeface
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RadioButton
import app.nikolaenko.andrew.taxisimple.R
import app.nikolaenko.andrew.taxisimple.model.Image
import app.nikolaenko.andrew.taxisimple.model.ServiceModel
import app.nikolaenko.andrew.taxisimple.model.addServiceModel
import com.bumptech.glide.Glide
import org.jetbrains.anko.find

class ServicesAdapter : RecyclerView.Adapter<ServicesAdapter.MyViewHolder>() {

    private var servicesList: List<addServiceModel> = listOf()

    fun setList(list: List<addServiceModel>) {
        servicesList = list
        notifyDataSetChanged()
    }

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var radioBtn: RadioButton = view.find(R.id.radio)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.service_item, parent, false)

        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val item = servicesList[position]

        val type = Typeface.createFromAsset(holder.itemView.context?.assets, "font/opensans_regular.ttf")

        holder.radioBtn.text = item.name
        holder.radioBtn.typeface = type

    }

    override fun getItemCount(): Int {
        return servicesList.size
    }
}
