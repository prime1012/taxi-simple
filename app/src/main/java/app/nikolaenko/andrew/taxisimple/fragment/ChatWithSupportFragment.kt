package app.nikolaenko.andrew.taxisimple.fragment

import android.app.Activity.RESULT_OK
import android.app.Dialog
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Typeface
import android.net.Uri
import android.os.Bundle
import android.os.SystemClock
import android.provider.MediaStore
import android.support.v4.content.ContextCompat
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import app.nikolaenko.andrew.taxisimple.R
import app.nikolaenko.andrew.taxisimple.model.DataResponse
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.chat_with_support_fragment.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.jetbrains.anko.find
import org.jetbrains.anko.support.v4.toast
import org.jetbrains.anko.textColor
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.OutputStream
import java.text.SimpleDateFormat
import java.util.*


class ChatWithSupportFragment : BaseFragment() {

    private var sendDisposable: Disposable? = null
    private var file: File? = null
    private var mLastClickTime = 0L

    override fun onStart() {
        super.onStart()
        changeFont()

        val formatter = SimpleDateFormat("dd.MM.yyyy")
        val date = formatter.format(Calendar.getInstance().timeInMillis)
        textCurrentDate.text = "$date год"

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.chat_with_support_fragment, container, false)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        leftBtn.setOnClickListener { goBack() }

        writeMessage.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (p0?.isNotEmpty()!!){
                    btnSend.visibility = View.VISIBLE
                } else {
                    btnSend.visibility = View.GONE
                }
            }

        })

        btnSend.setOnClickListener {

            if (SystemClock.elapsedRealtime() - mLastClickTime < 1000){
                return@setOnClickListener
            }
            mLastClickTime = SystemClock.elapsedRealtime()

            if ( textTheme.text.isEmpty()){
                toast("Введите название темы!")
                return@setOnClickListener
            }else{
                if (file != null ) {
                    sendReportWithImage(loginService.value.accessToken, textTheme.text.toString().trim(), writeMessage.text.toString().trim())
                } else {
                    createReport(loginService.value.accessToken, textTheme.text.toString().trim(), writeMessage.text.toString().trim())
                }
            }
        }

        addContent.setOnClickListener {
            showDialogAction("Добавить фото", "Галерея", "Камера", "add_photo")
        }
    }

    private fun createReport(token: String, topic: String, message: String){

        showProgress(true)

        sendDisposable = taxiService.value.createReport(token, topic, message)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ t: DataResponse ->

                    if (t.status == "success"){

                        writeMessage.text.clear()
                        writeMessage.clearFocus()
                        hideKeyboardFrom(context!!, writeMessage)
                        toast("Вы отправили обращение")

                    }

                    showProgress(false)
                    goBack()

                }, { e ->
                    e.printStackTrace()
                    toast(e.localizedMessage)
                    showProgress(false)
                })
    }

    private fun sendReportWithImage(token: String, topic: String, message: String) {

        writeMessage.text.clear()
        writeMessage.clearFocus()
        hideKeyboardFrom(context!!, writeMessage)

        val body = MultipartBody.Part.createFormData("token", token)
        val body2 = MultipartBody.Part.createFormData("topic", topic)
        val body3 = MultipartBody.Part.createFormData("text", message)
        val requestFile = RequestBody.create(MediaType.parse("image/png"), file)
        val body4 = MultipartBody.Part.createFormData("file", "image.jpg", requestFile)

        sendDisposable = taxiService.value.createReportWithImage(body, body2, body3, body4)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ t: DataResponse ->

                    if (t.status == "success"){

                        if (file != null){

                            file?.deleteOnExit()
                            file = null
                        }

                        goBack()
                    }

                }, { e ->
                    e.printStackTrace()
                    toast(e.localizedMessage)
                    showProgress(false)
                })
    }

    private fun showDialogAction(dialogTitle: String, firstBtn: String, secondBtn: String, action: String) {

        val mDialog = Dialog(context, R.style.CustomDialog)
        mDialog.setCancelable(true)
        mDialog.setContentView(R.layout.dialog_with_action)


        val type = Typeface.createFromAsset(context?.assets, "font/opensans_regular.ttf")

        val title = mDialog.find<TextView>(R.id.title)
        title.typeface = type
        title.text = dialogTitle
        val firstButtonText = mDialog?.find<TextView>(R.id.firstButtonText)
        firstButtonText.typeface = type
        firstButtonText.text = firstBtn
        val secondButtonText = mDialog?.find<TextView>(R.id.secondButtonText)
        secondButtonText.typeface = type
        secondButtonText.text = secondBtn
        secondButtonText.textColor = ContextCompat.getColor(context!!, R.color.black)

        firstButtonText.setOnClickListener {
            mDialog.dismiss()

            val intent = Intent(Intent.ACTION_GET_CONTENT)
            intent.type = "image/jpeg"
            try {
                startActivityForResult(intent, GALLERY_REQUEST_CODE)
            } catch (e: ActivityNotFoundException) {
                e.printStackTrace()
            }
        }
        secondButtonText.setOnClickListener {
            mDialog.dismiss()
            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            startActivityForResult(intent, REQUEST_IMAGE)
        }

        mDialog.show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == GALLERY_REQUEST_CODE){

            if (resultCode == RESULT_OK) {
                try {
                    file = File(context!!.filesDir, "img" + Date().time)
                    val cr = mainActivity.contentResolver
                    val `is` = cr.openInputStream(data!!.data!!)
                    file!!.createNewFile()
                    `is`.copyTo(file!!.outputStream())
                    file!!.absolutePath

                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }
        }

        if (requestCode == REQUEST_IMAGE && resultCode == RESULT_OK) {
            val photo = data?.extras?.get("data") as Bitmap
            file = File(bitmapToFile(photo).encodedPath)
        }

    }

    private fun bitmapToFile(bitmap: Bitmap): Uri {
        val wrapper = ContextWrapper(context)

        // Initialize a new file instance to save bitmap object
        var file = wrapper.getDir("Images", Context.MODE_PRIVATE)
        file = File(file,"${UUID.randomUUID()}.jpg")

        try{
            // Compress the bitmap and save in jpg format
            val stream: OutputStream = FileOutputStream(file)
            bitmap.compress(Bitmap.CompressFormat.JPEG,100,stream)
            stream.flush()
            stream.close()
        }catch (e:IOException){
            e.printStackTrace()
        }

        // Return the saved bitmap uri
        return Uri.parse(file.absolutePath)
    }

    private fun changeFont() {
        changeFontInTextView(textTitle)
        changeFontInTextView(textWrite)
        changeFontInTextView(textCurrentDate)
        changeFontInEditTextView(writeMessage)
        changeFontInEditTextViewBold(textTheme)
    }

    companion object {
        private val GALLERY_REQUEST_CODE = 100
        private val REQUEST_IMAGE = 200
    }

}
