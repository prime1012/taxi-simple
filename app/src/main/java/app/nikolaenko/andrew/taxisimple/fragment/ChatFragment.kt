package app.nikolaenko.andrew.taxisimple.fragment

import android.os.Bundle
import android.os.SystemClock
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import app.nikolaenko.andrew.taxisimple.R
import app.nikolaenko.andrew.taxisimple.adapter.ChatAdapter
import app.nikolaenko.andrew.taxisimple.model.ChatResponse
import app.nikolaenko.andrew.taxisimple.model.MessageModel
import app.nikolaenko.andrew.taxisimple.model.MessagesResponse
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.chat_fragment.*
import org.jetbrains.anko.support.v4.toast


class ChatFragment : BaseFragment() {

    private lateinit var chatAdapter: ChatAdapter

    private var userId: Int? = null

    private var disposableChatList: Disposable? = null
    private var disposableChatSend: Disposable? = null

    private var mLastClickTime = 0L

    private var limit: Int = 10
    private var offset: Int = 0
    private var order = "dsc"

    override fun onStart() {
        super.onStart()
        changeFont()

        showProgress(true)
        userId = arguments?.getInt(ARG_USER_ID)
        getMessagesList(limit, offset, order)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.chat_fragment, container, false)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        chatAdapter = ChatAdapter()
        rvChat.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, true)
        rvChat.itemAnimator = DefaultItemAnimator()
        rvChat.adapter = chatAdapter
        rvChat.isNestedScrollingEnabled = false

        leftBtn.setOnClickListener { goBack() }

        writeMessage.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (p0?.isNotEmpty()!!){
                    btnSend.visibility = View.VISIBLE
                } else {
                    btnSend.visibility = View.GONE
                }
            }

        })

        btnSend.setOnClickListener {

            if (SystemClock.elapsedRealtime() - mLastClickTime > 1000){

                if (writeMessage.text.isEmpty()){
                    return@setOnClickListener
                } else {
                    val text = writeMessage.text.toString().trim()
                    sendMessage(userId!!, text)
                }
            }
            mLastClickTime = SystemClock.elapsedRealtime()

        }

        swipyrefreshlayout.setOnRefreshListener { direction ->
            if (direction == SwipyRefreshLayoutDirection.TOP) {

                getPreviousMessages(chatAdapter.itemCount + 10, chatAdapter.itemCount)

            } else {
                getMessagesList(10, 0, order)
            }
        }
    }

    private var messageList: ArrayList<MessageModel> = arrayListOf()

    private fun getMessagesList(limit: Int, offset: Int, ordered: String){

        disposableChatList = taxiService.value.getMessageList(loginService.value.accessToken, limit, offset, ordered)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ t: MessagesResponse ->

                    if (t.messages != null){

                        messageList = (t.messages as ArrayList<MessageModel>?)!!
                        chatAdapter.setList(messageList, userId!!)

                    } else {
                        toast("Чат пока пустой")
                    }

                    swipyrefreshlayout.isRefreshing = false
                    showProgress(false)

                }, { e ->
                    e.printStackTrace()
                    toast(e.localizedMessage)
                    showProgress(false)
                })
    }

    private fun getPreviousMessages(limit: Int, offset: Int){

        disposableChatList = taxiService.value.getMessageList(loginService.value.accessToken, limit, offset, order)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ t: MessagesResponse ->

                    if (t.messages != null){

                        val neMessageList = (t.messages as ArrayList<MessageModel>?)!!
                        messageList.addAll(neMessageList)
                        chatAdapter.setList(messageList, userId!!)

                    }

                    swipyrefreshlayout.isRefreshing = false

                }, { e ->
                    e.printStackTrace()
                    toast(e.localizedMessage)
                })

    }

    private fun sendMessage(userId: Int, message: String){

        disposableChatSend = taxiService.value.sendMessage(loginService.value.accessToken, userId, message)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ t: ChatResponse ->

                    if (t.resultChat == "ok"){
                        writeMessage.text.clear()
                        writeMessage.clearFocus()
                        hideKeyboardFrom(context!!, writeMessage)
                        getMessagesList(limit, offset, order)
                    }

                }, { e ->
                    e.printStackTrace()
                    toast(e.localizedMessage)
                    showProgress(false)
                })
    }

    private fun changeFont(){
        changeFontInTextView(textTitle)
        changeFontInTextView(textWrite)
        changeFontInEditTextView(writeMessage)
    }

    override fun onStop() {
        super.onStop()
        disposableChatList?.dispose()
        disposableChatSend?.dispose()
    }

    companion object {
        val ARG_USER_ID = "arg_user_id"

        fun newInstance(id: Int): ChatFragment {

            val args = Bundle()

            args.putInt(ARG_USER_ID, id)
            val fragment = ChatFragment()
            fragment.arguments = args
            return fragment
        }
    }
}
