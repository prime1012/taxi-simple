package app.nikolaenko.andrew.taxisimple.model

import com.google.gson.annotations.SerializedName

class MessagesResponse {

    @SerializedName("status")
    var status: String? = null

    @SerializedName("messages")
    var messages: List<MessageModel>? = null
}