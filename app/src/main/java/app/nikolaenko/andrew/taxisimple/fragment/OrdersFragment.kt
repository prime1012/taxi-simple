package app.nikolaenko.andrew.taxisimple.fragment

import android.annotation.SuppressLint
import android.graphics.Typeface
import android.location.Location
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.DialogFragment
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import app.nikolaenko.andrew.taxisimple.R
import app.nikolaenko.andrew.taxisimple.dialog.OrderDialog
import app.nikolaenko.andrew.taxisimple.model.DataResponse
import app.nikolaenko.andrew.taxisimple.model.OrderResponse
import app.nikolaenko.andrew.taxisimple.model.OrderTypeModel
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationListener
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.model.*
import com.pixplicity.easyprefs.library.Prefs
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.orders_fragment.*
import org.jetbrains.anko.support.v4.toast
import org.jetbrains.anko.textColor
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices


class OrdersFragment : BaseFragment(), GoogleMap.OnMarkerClickListener,  GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    var mLocationRequest: LocationRequest? = null
    var mGoogleApiClient: GoogleApiClient? = null
    var mLastLocation: Location? = null

    private lateinit var fusedLocationClient: FusedLocationProviderClient

    private var googleMap: GoogleMap? = null

    private var marker: Marker? = null

    private var orderDisposable: Disposable? = null
    private var orderTypeDisposable: Disposable? = null
    private var ordersListDisposable: Disposable? = null

    private var type: Int? = null

    override fun onStart() {
        super.onStart()

        changeFonts()

        if (isSetTarif){
            rbFromBoard.isEnabled = true
            rbFromBoard.isChecked = true
        } else {
            rbFromBoard.isEnabled = false
            rbFromBoard.isChecked = false
        }

        getOrderType()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.orders_fragment, container, false)


        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(mainActivity)

    }

    @SuppressLint("MissingPermission")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        rbFromBoard.typeface = Typeface.createFromAsset(context?.assets, "font/opensans_bold.ttf")

        mapView?.onCreate(savedInstanceState)

        mapView?.onResume()

        try {
            MapsInitializer.initialize(activity!!.applicationContext)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        mapView?.getMapAsync { mMap ->
            googleMap = mMap

            googleMap?.uiSettings?.isRotateGesturesEnabled = false
            googleMap?.uiSettings?.isCompassEnabled = false
            googleMap?.uiSettings?.isMapToolbarEnabled = false
            googleMap?.uiSettings?.isIndoorLevelPickerEnabled = true
            googleMap?.uiSettings?.isMyLocationButtonEnabled = true

            if (mGoogleApiClient != null && mGoogleApiClient!!.isConnected) {
                mGoogleApiClient?.stopAutoManage(activity!!)
                mGoogleApiClient?.disconnect()

                buildGoogleApiClient()
            }

            googleMap?.isMyLocationEnabled = true

            googleMap?.setOnMarkerClickListener(this)

            googleMap?.setOnMapLoadedCallback {

                fusedLocationClient.lastLocation
                        .addOnSuccessListener { location: Location? ->

                            if (location != null){
                                googleMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(location!!.latitude, location.longitude), 16f))
                                Prefs.putDouble("my_latitude", location.latitude)
                                Prefs.putDouble("my_longitude", location.longitude)
                            } else {
                                toast(getString(R.string.cant_get_location))
                                googleMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(55.751244, 37.618423), 16f))
                                Prefs.putDouble("my_latitude", 55.751244)
                                Prefs.putDouble("my_longitude", 37.618423)
                            }
                        }

                googleMap?.setOnMarkerClickListener(this)

                getOrdersOnMap()

                val handler = Handler()
                handler.postDelayed({

                    if (Prefs.getBoolean("order_taked", false)){
                        (fragmentManager?.findFragmentByTag("dialog_order") as? DialogFragment)?.dismiss()
                        OrderDialog().show(fragmentManager, "dialog_order")
                    }

                }, 2000)

            }
        }

        menuBtn.setOnClickListener {
            mainActivity.hardReplaceFragment(DriverMainFragment())
        }

        historyBtn.setOnClickListener {
            replaceFragment(MoneyFragment.newInstance(true), true)
        }

        fromBoardBtn.setOnClickListener {
            if (rbFromBoard.isEnabled && rbFromBoard.isChecked){
                rbFromBoard.isEnabled = false
                rbFromBoard.isChecked = false
                isSetTarif = false
            } else {
                replaceFragment(FromBoardFragment(), true)
            }
        }

        automaticLayout.setOnClickListener {
            if (type == 1){
                return@setOnClickListener
            } else {
                setOrderType(1)
            }

        }

        manualLayout.setOnClickListener {
            if (type == 0){
                return@setOnClickListener
            } else {
                setOrderType(0)
            }
        }

        cancelAuto.setOnClickListener {
            setOrderType(0)
        }
    }

    override fun onMarkerClick(marker: Marker): Boolean {

        if (marker == marker) {
            addFragment(OrderInfoFragment.newInstance(marker.tag.toString()), true)
        }

        return false
    }

    override fun onResume() {
        super.onResume()
        mapView?.onResume()

        googleMap?.setOnMapLoadedCallback {
            getOrdersOnMap()
        }
    }

    override fun onPause() {
        super.onPause()
        mapView?.onPause()

        if (mGoogleApiClient != null && mGoogleApiClient!!.isConnected) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this)
            mGoogleApiClient?.stopAutoManage(activity!!)
            mGoogleApiClient?.disconnect()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        mapView?.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapView?.onLowMemory()
    }

    private fun getOrderType(){

        orderDisposable = taxiService.value.getAutoOrder(loginService.value.accessToken)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ t: OrderTypeModel ->

                    if (t.status == "success"){

                        type = t.option

                        when(type){
                            0-> {
                                manualText.textColor = ContextCompat.getColor(context!!, R.color.main_orange)
                                manualImg.setImageResource(R.drawable.hand_icon)
                                automaticText.textColor = ContextCompat.getColor(context!!, R.color.main_gray)
                                automaticImg.setImageResource(R.drawable.autom_icon_not_active)

                                searchLayout.visibility = View.GONE
                                menuBtn.visibility = View.VISIBLE
                            }
                            else -> {
                                automaticText.textColor = ContextCompat.getColor(context!!, R.color.main_orange)
                                automaticImg.setImageResource(R.drawable.autom_icon)
                                manualText.textColor = ContextCompat.getColor(context!!, R.color.main_gray)
                                manualImg.setImageResource(R.drawable.hand_not_active)

                                searchLayout.visibility = View.VISIBLE
                                menuBtn.visibility = View.GONE
                            }
                        }
                    }

                }, { e ->
                    e.printStackTrace()
                    toast(e.localizedMessage)
                })
    }

    private fun setOrderType(type: Int){

        orderTypeDisposable = taxiService.value.setAutoOrder(loginService.value.accessToken, type)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ t: DataResponse ->

                    if (t.status == "success"){
                        getOrderType()
                    }

                }, { e ->
                    e.printStackTrace()
                    toast(e.localizedMessage)
                })
    }

    @SuppressLint("MissingPermission")
    private fun getOrdersOnMap(){

        ordersListDisposable = taxiService.value.getOrdersOnMap(loginService.value.accessToken)

                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ t: OrderResponse ->

                    if (t.status == "success"){

                        if (t.ordersOnMap != null) {

                            val list = t.ordersOnMap

                            for (i in 0 until list!!.size) {

                                marker =  googleMap!!.addMarker(MarkerOptions().position(LatLng(list[i].originLatitude!!.toDouble(), list[i].originLongitude!!.toDouble())).icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_icon)))
                                marker?.tag = list[i].orderId
                            }

                        }
                    }

                }, { e ->
                    e.printStackTrace()
                    toast(e.localizedMessage)
                })
    }

    private fun changeFonts(){
        changeFontInTextViewBold(textHistory)
        changeFontInTextViewBold(text)

        changeFontInTextView(automaticText)
        changeFontInTextView(manualText)
        changeFontInTextView(declText)
        changeFontInTextView(text2)
    }

    @SuppressLint("MissingPermission")
    override fun onConnected(p0: Bundle?) {
        mLocationRequest = LocationRequest()
        mLocationRequest?.interval = 10000
        mLocationRequest?.fastestInterval = 5000
        mLocationRequest?.priority = LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this)
    }

    override fun onConnectionSuspended(p0: Int) {

    }

    override fun onConnectionFailed(p0: ConnectionResult) {

    }

    override fun onLocationChanged(p0: Location?) {
        mLastLocation = p0

        Prefs.putDouble("my_latitude", mLastLocation!!.latitude)
        Prefs.putDouble("my_longitude", mLastLocation!!.longitude)

        getOrdersOnMap()

//        googleMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(p0!!.latitude, p0.longitude), 16f))
    }

    @Synchronized
    protected fun buildGoogleApiClient() {
        mGoogleApiClient = GoogleApiClient.Builder(context!!)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build()
        mGoogleApiClient?.connect()
    }


    override fun onStop() {
        super.onStop()
        orderTypeDisposable?.dispose()
        orderDisposable?.dispose()
        ordersListDisposable?.dispose()
    }

    companion object {
        var isSetTarif: Boolean = false
    }
}
