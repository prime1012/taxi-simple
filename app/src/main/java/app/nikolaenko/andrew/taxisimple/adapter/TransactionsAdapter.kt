package app.nikolaenko.andrew.taxisimple.adapter


import android.graphics.Typeface
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import app.nikolaenko.andrew.taxisimple.R
import app.nikolaenko.andrew.taxisimple.model.Transaction
import org.jetbrains.anko.find
import org.jetbrains.anko.textColor

class TransactionsAdapter : RecyclerView.Adapter<TransactionsAdapter.MyViewHolder>() {

    private var transactionsList: List<Transaction> = listOf()

    fun setList(list: List<Transaction>) {
        transactionsList = list
        notifyDataSetChanged()
    }

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var textValue: TextView = view.find(R.id.textValue)
        var textCurrency: TextView = view.find(R.id.textCurrency)
        var textDate: TextView = view.find(R.id.textDate)
        var textCommentary: TextView = view.find(R.id.textCommentary)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.transaction_item, parent, false)

        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val transactionItem = transactionsList[position]

        val type = Typeface.createFromAsset(holder.itemView.context?.assets, "font/opensans_regular.ttf")
        val typeBold = Typeface.createFromAsset(holder.itemView.context?.assets, "font/opensans_bold.ttf")

        when(transactionItem.type){
            0 -> {
                holder.textValue.textColor = ContextCompat.getColor(holder.itemView.context, R.color.main_gray)
                holder.textCurrency.textColor = ContextCompat.getColor(holder.itemView.context, R.color.main_gray)

                holder.textValue.text = "- " + transactionItem.amount.toString()
                holder.textCurrency.text = "руб"
            }

            else-> {
                holder.textValue.textColor = ContextCompat.getColor(holder.itemView.context, R.color.main_green)
                holder.textCurrency.textColor = ContextCompat.getColor(holder.itemView.context, R.color.main_green)

                holder.textValue.text = "+ " + transactionItem.amount.toString()
                holder.textCurrency.text = "руб"
            }
        }

        holder.textDate.text = transactionItem.timestamp
        holder.textCommentary.text = transactionItem.description

        holder.textValue.typeface = typeBold
        holder.textCurrency.typeface = type
        holder.textCommentary.typeface = type
        holder.textDate.typeface = typeBold
    }

    override fun getItemCount(): Int {
        return transactionsList.size
    }
}
