package app.nikolaenko.andrew.taxisimple.Notification

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.support.v4.content.LocalBroadcastManager
import android.text.TextUtils
import android.util.Log

import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import android.R.attr.smallIcon
import android.app.Notification
import android.support.v4.app.NotificationCompat
import app.nikolaenko.andrew.taxisimple.R
import android.app.NotificationManager
import android.graphics.BitmapFactory
import android.app.PendingIntent
import android.support.v4.app.TaskStackBuilder
import app.nikolaenko.andrew.taxisimple.activity.MainActivity




class MyFirebaseMessagingService : FirebaseMessagingService() {

    private var notificationUtils: NotificationUtils? = null

    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        Log.e(TAG, "From: " + remoteMessage!!.from!!)

        val CHANNEL_ID = "my_channel_01"
        val mBuilder = NotificationCompat.Builder(this).setChannelId(CHANNEL_ID)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("My notification")
                .setContentText("Hello World!")

        val resultIntent = Intent(this, MainActivity::class.java)
        val stackBuilder = TaskStackBuilder.create(this)
        stackBuilder.addParentStack(MainActivity::class.java)
        stackBuilder.addNextIntent(resultIntent)
        val resultPendingIntent = stackBuilder.getPendingIntent(
                0,
                PendingIntent.FLAG_UPDATE_CURRENT
        )
        mBuilder.setContentIntent(resultPendingIntent)
        val mNotificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        mNotificationManager.notify(0, mBuilder.build())
    }

    private fun handleNotification(message: String?) {
        if (!NotificationUtils.isAppIsInBackground(applicationContext)) {
            // app is in foreground, broadcast the push message
            val pushNotification = Intent(Config.PUSH_NOTIFICATION)
            pushNotification.putExtra("message", message)
            LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification)

            // play notification sound
            val notificationUtils = NotificationUtils(applicationContext)
            notificationUtils.playNotificationSound()
        } else {
            // If the app is in background, firebase itself handles the notification
        }
    }

    private fun handleDataMessage(json: JSONObject) {
        Log.e(TAG, "push json: " + json.toString())

        try {
            val data = json.getJSONObject("data")

            val title = data.getString("title")
            val message = if (data.isNull("message")) "" else data.getString("message")
            val isBackground = data.getBoolean("is_background")
            val repeatSound = false
            val imageUrl = data.getString("image")
            val timestamp = data.getString("timestamp")
            val payload = data.getJSONObject("payload")

            Log.e(TAG, "title: $title")
            Log.e(TAG, "message: $message")
            Log.e(TAG, "isBackground: $isBackground")
            Log.e(TAG, "payload: " + payload.toString())
            Log.e(TAG, "imageUrl: $imageUrl")
            Log.e(TAG, "timestamp: $timestamp")

            val pushNotification = Intent(payload.getString("action"))
            pushNotification.putExtra("payload", payload.toString())
            LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification)

            //Получение нового сообщение
            if (payload.getString("action").contains(Config.PUSH_NEW_MESSAGE)) {
                val messagesClient = getSharedPreferences("messagesClient", Context.MODE_PRIVATE)
                val newMessage = JSONObject()
                var messagesJsonArray = JSONArray()

                if (messagesClient.getString("messages", null) != null)
                    messagesJsonArray = JSONArray(messagesClient.getString("messages", null))

                newMessage.put("userId", payload.getInt("userId"))
                newMessage.put("text", payload.getString("text"))
                newMessage.put("timestamp", payload.getLong("timestamp"))
                messagesJsonArray.put(newMessage)
                messagesClient.edit().putString("messages", messagesJsonArray.toString()).apply()
            }

            //            if(payload.getString("action").contains(Config.PUSH_ORDER_CANCELED)) {
            //                getSharedPreferences("activeOrder", 0).edit().clear().apply();
            //                getSharedPreferences("messagesClient", 0).edit().clear().apply();
            //                Config.setInfoPerformer(this,"status", 1);
            //            }
            //
            //            if(payload.getString("action").contains(Config.PUSH_PERFORMERS_STATUS_CHANGED))
            //                getSharedPreferences("activeOrder", 0).edit().putString("performers", payload.getJSONArray("performers").toString()).apply();
            //
            //            if(payload.getString("action").contains(Config.PUSH_PERFORMER_LOCK_STATUS))
            //                Config.setInfoPerformer(this,"status", payload.getInt("status"));
            //
            //            if(payload.getString("action").contains(Config.PUSH_PERFORMER_ACCESS_STATUS))
            //                Config.setInfoPerformer(this,"validate", payload.getBoolean("validate"));
            //
            //            if(payload.getString("action").contains(Config.PUSH_NEW_ORDER))
            //                repeatSound = true;

            if (!isBackground) {
                // check for image attachment
                if (TextUtils.isEmpty(imageUrl)) {
                    showNotificationMessage(applicationContext, title, message, timestamp, repeatSound)
                } else {
                    // image is present, show notification with image
                    showNotificationMessageWithBigImage(applicationContext, title, message, timestamp, imageUrl, repeatSound)
                }
            }
        } catch (e: JSONException) {
            Log.e(TAG, "Json Exception: " + e.message)
        } catch (e: Exception) {
            Log.e(TAG, "Exception: " + e.message)
        }

    }

    /**
     * Showing notification with text only
     */
    private fun showNotificationMessage(context: Context, title: String, message: String, timeStamp: String, repeatSound: Boolean) {
        notificationUtils = NotificationUtils(context)
        notificationUtils!!.showNotificationMessage(title, message, timeStamp, repeatSound)
    }

    /**
     * Showing notification with text and image
     */
    private fun showNotificationMessageWithBigImage(context: Context, title: String, message: String, timeStamp: String, imageUrl: String, repeatSound: Boolean) {
        notificationUtils = NotificationUtils(context)
        notificationUtils!!.showNotificationMessage(title, message, timeStamp, imageUrl, repeatSound)
    }

    companion object {

        private val TAG = MyFirebaseMessagingService::class.java.simpleName
    }
}