package app.nikolaenko.andrew.taxisimple.fragment

import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import app.nikolaenko.andrew.taxisimple.R
import app.nikolaenko.andrew.taxisimple.adapter.ServicesAdapter
import app.nikolaenko.andrew.taxisimple.dialog.OrderDialog
import app.nikolaenko.andrew.taxisimple.model.DataResponse
import app.nikolaenko.andrew.taxisimple.model.DriverModel
import app.nikolaenko.andrew.taxisimple.model.OrderInfoResponse
import com.google.android.flexbox.FlexDirection
import com.google.android.flexbox.FlexWrap
import com.google.android.flexbox.FlexboxLayoutManager
import com.google.android.flexbox.JustifyContent
import com.pixplicity.easyprefs.library.Prefs
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.order_info_fragment.*
import org.jetbrains.anko.support.v4.toast


class OrderInfoFragment : BaseFragment() {

    private lateinit var adapter: ServicesAdapter

    private var orderId: String = ""

    private var infoDisposable: Disposable? = null
    private var takeOrderDisposable: Disposable? = null
    private var driverDisposable: Disposable? = null

    private var isCanTakeOrder: Boolean = false

    override fun onStart() {
        super.onStart()
        changeFont()

        orderId = arguments?.getString(ARG_ORDER_ID)!!

//        getOrderDetails(orderId.toInt())
        getOrderDetails()
        checkAvailable()

//        val list = ArrayList<ServiceModel>()
//        list.clear()
//        list.add(0, ServiceModel("Детское кресло"))
//        list.add(1, ServiceModel("Курящий салон"))
//        list.add(2, ServiceModel("Кондиционер или климат контроль"))
//        adapter.setList(list)

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.order_info_fragment, container, false)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val layoutManager = FlexboxLayoutManager(context, FlexDirection.ROW, FlexWrap.WRAP)
        layoutManager.justifyContent = JustifyContent.FLEX_START
        rvServices.layoutManager = layoutManager
        adapter = ServicesAdapter()
        rvServices.adapter = adapter
        rvServices.isNestedScrollingEnabled = false

        leftBtn.setOnClickListener {
            goBack()
        }

        btnSubmit.setOnClickListener {

            if (isCanTakeOrder){
                toast("У вас есть незавершенный заказ!!!")
                return@setOnClickListener
            }

            takeOrderDisposable = taxiService.value.confirmOrder(loginService.value.accessToken, orderId.toInt())
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({ t: DataResponse ->

                        if (t.status == "success"){

                            Prefs.putBoolean("order_taked", true)
                            Prefs.putInt("order_id", orderId.toInt())

                            goBack()
                        }

                    }, { e ->
                        e.printStackTrace()
                        showProgress(false)
                        toast(e.localizedMessage)
                    })
        }
    }

    private fun changeFont(){
        changeFontInTextView(textTitle)
        changeFontInTextView(textDate)
        changeFontInTextView(text)
        changeFontInTextView(textTarif)
        changeFontInTextView(textCurrency)
        changeFontInTextView(textSubmit)
        changeFontInTextView(text2)
        changeFontInTextView(textTo)
        changeFontInTextView(textFrom)

        changeFontInTextViewBold(textTime)
        changeFontInTextViewBold(textType)
        changeFontInTextViewBold(textValue)
    }

    override fun onDetach() {
        super.onDetach()

        if (Prefs.getBoolean("order_taked", false)){
            (fragmentManager?.findFragmentByTag("dialog_order") as? DialogFragment)?.dismiss()
            OrderDialog().show(fragmentManager, "dialog_order")
        }
    }

    private fun checkAvailable(){

        driverDisposable = taxiService.value.getDriverInfo(loginService.value.accessToken)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ t: DriverModel ->

                    isCanTakeOrder = t.hasActiveOrder!!

                }, { e ->
                    e.printStackTrace()
                    showProgress(false)
                })
    }

//    private fun getOrderDetails(oderId: Int){
    private fun getOrderDetails(){
        showProgress(true)
//        infoDisposable = taxiService.value.getOrderInfo(loginService.value.accessToken, oderId)
        infoDisposable = taxiService.value.getOrderInfo(loginService.value.accessToken)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ t: OrderInfoResponse ->

                    if (t.status == "success"){

                        if (t.orderInfo != null){

                            when(t.orderInfo!!.transportationTariffId){
                                16-> {textType.text = getString(R.string.tarif_ekonom)}
                                17-> {textType.text = getString(R.string.tarif_comfort)}
                                18-> {textType.text = getString(R.string.tarif_busines)}
                                19-> {textType.text = getString(R.string.tarif_vip)}
                            }

                            t.orderInfo!!.resultTripCost?.let { textValue.text = it.toString() }
                            t.orderInfo!!.originText?.let { textFrom.text = it }
                            t.orderInfo!!.destinationText?.let { textTo.text = it }

                            val time = t.orderInfo!!.createdAt.toString()

                            val s = time.substring(0, Math.min(time.length, 10))
                            val strNew = s.replace("-", ".")

                            val strNew2 = time.replace(s+ " ", "")
                            textDate.text = strNew
                            textTime.text = strNew2

                            adapter.setList(t.orderInfo!!.services!!)

                            val paymentType = t.orderInfo!!.paymentType
                            when(paymentType){
                                1->{
                                    text2.text = getString(R.string.payment_type_nal)
                                    imagePayment.setImageResource(R.drawable.nal_icon)
                                }
                                2->{
                                    text2.text = getString(R.string.payment_type_bez_nal)
                                    imagePayment.setImageResource(R.drawable.visa_icon)
                                }
                                3->{
                                    text2.text = getString(R.string.payment_type_virtual)
                                    imagePayment.setImageResource(R.drawable.nal_icon)
                                }
                            }

                        }

                    }

                    showProgress(false)
                }, { e ->
                    e.printStackTrace()
                    showProgress(false)
                })

    }

    override fun onStop() {
        super.onStop()
        infoDisposable?.dispose()
        takeOrderDisposable?.dispose()
    }


    companion object {

        private val ARG_ORDER_ID = "arg_order_id"

        fun newInstance(orderId: String): OrderInfoFragment {

            val args = Bundle()

            args.putString(ARG_ORDER_ID, orderId)
            val fragment = OrderInfoFragment()
            fragment.arguments = args
            return fragment
        }
    }
}
