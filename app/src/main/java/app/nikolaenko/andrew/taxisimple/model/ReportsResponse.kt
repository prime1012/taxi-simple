package app.nikolaenko.andrew.taxisimple.model

import com.google.gson.annotations.SerializedName

class ReportsResponse {
    @SerializedName("reports")
    var reports: List<Report>? = null
}