package app.nikolaenko.andrew.taxisimple.model

import com.google.gson.annotations.SerializedName

class UserModel {

    @SerializedName("id")
    var id: Int? = null

    @SerializedName("name")
    var name: String? = null

    @SerializedName("email")
    var email: String? = null

    @SerializedName("position")
    var position: Int? = null
}