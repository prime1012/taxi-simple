package app.nikolaenko.andrew.taxisimple.model

import com.google.gson.annotations.SerializedName

class PerformerModel {

    @SerializedName("id")
    var id: Int? = null

    @SerializedName("full_name")
    var fullName: NameModel? = null
}