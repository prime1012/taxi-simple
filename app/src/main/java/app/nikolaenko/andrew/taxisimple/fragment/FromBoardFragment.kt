package app.nikolaenko.andrew.taxisimple.fragment

import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import app.nikolaenko.andrew.taxisimple.R
import app.nikolaenko.andrew.taxisimple.adapter.TarifAdapter
import app.nikolaenko.andrew.taxisimple.model.TarifModel
import kotlinx.android.synthetic.main.from_board_fragment.*
import org.jetbrains.anko.support.v4.toast


class FromBoardFragment : BaseFragment() {

    private lateinit var adapter: TarifAdapter

    private var pos: Int = -1

    override fun onStart() {
        super.onStart()
        changeFont()

        val list = ArrayList<TarifModel>()
        list.clear()
        list.add(0, TarifModel(R.drawable.business, "Бизнес", "Audi A6 - XX2018XX", false))
        list.add(1, TarifModel(R.drawable.comfort, "Комфорт", "Audi A6 - XX2018XX", true))
        list.add(2, TarifModel(R.drawable.econom, "Эконом", "Audi A6 - XX2018XX", false))
        list.add(3, TarifModel(R.drawable.vip, "VIP", "Audi A6 - XX2018XX", false))
        adapter.setList(list)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.from_board_fragment, container, false)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        leftBtn.setOnClickListener {
            goBack()
        }

        rightBtn.setOnClickListener {

            if (pos != -1){
                OrdersFragment.isSetTarif = true
                mainActivity.hardReplaceFragment(OrdersFragment())
            } else {
                toast("Выберите сначала тариф!")
            }
        }

        adapter = TarifAdapter {position ->
            pos = adapter.getActiveItem()
            pos = position
        }
        rvTarif.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        rvTarif.itemAnimator = DefaultItemAnimator()
        rvTarif.adapter = adapter
        rvTarif.isNestedScrollingEnabled = false

        btnSubmit.setOnClickListener {
            if (pos != -1){
                OrdersFragment.isSetTarif = true
                mainActivity.hardReplaceFragment(OrdersFragment())
            } else {
                toast("Выберите сначала тариф!")
            }
        }
    }

    private fun changeFont(){
        changeFontInTextView(textTitle)
        changeFontInTextView(textSubmit)
    }
}
