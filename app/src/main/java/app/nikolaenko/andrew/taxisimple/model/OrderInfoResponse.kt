package app.nikolaenko.andrew.taxisimple.model

import com.google.gson.annotations.SerializedName

class OrderInfoResponse {

    @SerializedName("status")
    var status: String? = null

    @SerializedName("order_info")
    var orderInfo: OrderInfoModel? = null
}