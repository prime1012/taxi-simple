package app.nikolaenko.andrew.taxisimple.model

import com.google.gson.annotations.SerializedName

class ReportObject {

    @SerializedName("report")
    var reports: Report? = null
}