package app.nikolaenko.andrew.taxisimple.model

import com.google.gson.annotations.SerializedName

class DriverLicenseModel {

    @SerializedName("number")
    var number: String? = null

    @SerializedName("series")
    var series: String? = null

    @SerializedName("dateOfIssue")
    var dateOfIssue: String? = null

    @SerializedName("dateOfExpiry")
    var dateOfExpiry: String? = null
}