package app.nikolaenko.andrew.taxisimple.model

import com.google.gson.annotations.SerializedName

class Report {

    @SerializedName("report_id")
    var id: Int? = null

    @SerializedName("account_type")
    var accountType: Int? = null

    @SerializedName("checked")
    var checked: Int? = null

    @SerializedName("topic")
    var topic: String? = null

    @SerializedName("timestamp")
    var timestamp: Long? = null

    @SerializedName("date")
    var date: String? = null

    @SerializedName("text")
    var text: String? = null

    @SerializedName("patch_file")
    var patchFile: String? = null

    @SerializedName("answers")
    var answer: List<AnswerModel>? = null
}