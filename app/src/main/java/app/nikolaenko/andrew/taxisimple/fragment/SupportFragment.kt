package app.nikolaenko.andrew.taxisimple.fragment

import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import app.nikolaenko.andrew.taxisimple.R
import app.nikolaenko.andrew.taxisimple.adapter.ReportsAdapter
import app.nikolaenko.andrew.taxisimple.model.ReportsResponse
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.support_fragment.*
import org.jetbrains.anko.support.v4.toast


class SupportFragment : BaseFragment() {

    private lateinit var adapter: ReportsAdapter
    private var reportsDisposable: Disposable? = null

    override fun onStart() {
        super.onStart()
        changeFont()

        getReportsList()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.support_fragment, container, false)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        leftBtn.setOnClickListener { goBack() }

        rightBtn.setOnClickListener { toast("phone") }

//        rbDisp.setOnClickListener {
//            rbDisp.isChecked = true
//            rbManag.isChecked = false
//        }
//
//        rbManag.setOnClickListener {
//            rbManag.isChecked = true
//            rbDisp.isChecked = false
//        }

        newMessageToSupport.setOnClickListener {
            replaceFragment(ChatWithSupportFragment(), true)
        }

        adapter = ReportsAdapter{ id ->
            replaceFragment(SupportThemeDetailsFragment.newInstance(id), true)
        }
        rvSupportThemes.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        rvSupportThemes.itemAnimator = DefaultItemAnimator()
        rvSupportThemes.adapter = adapter
        rvSupportThemes.isNestedScrollingEnabled = false
    }

    private fun changeFont() {
        changeFontInTextView(textTitle)
        changeFontInTextView(text)
//        rbDisp.typeface = Typeface.createFromAsset(context?.assets, "font/opensans_regular.ttf")
//        rbManag.typeface = Typeface.createFromAsset(context?.assets, "font/opensans_regular.ttf")
    }

    private fun getReportsList(){

        showProgress(true)

        reportsDisposable = taxiService.value.getReports(loginService.value.accessToken)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ t: ReportsResponse ->

                    if (t.reports!!.isEmpty()){
                        toast("Тут пока ничего нету")
                    } else {
                        adapter.setList(t.reports!!)
                    }
                    showProgress(false)
                }, { e ->
                    e.printStackTrace()
                    toast(e.localizedMessage)
                    showProgress(false)
                })
    }

    override fun onStop() {
        super.onStop()
        reportsDisposable?.dispose()
    }

}
