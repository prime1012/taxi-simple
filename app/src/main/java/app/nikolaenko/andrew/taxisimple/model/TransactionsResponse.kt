package app.nikolaenko.andrew.taxisimple.model

import com.google.gson.annotations.SerializedName

class TransactionsResponse {

    @SerializedName("status")
    var status: String? = null

    @SerializedName("transactions")
    var transactions: List<Transaction>? = null
}