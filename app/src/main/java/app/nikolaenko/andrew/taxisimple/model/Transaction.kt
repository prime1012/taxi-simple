package app.nikolaenko.andrew.taxisimple.model

import com.google.gson.annotations.SerializedName

class Transaction {

    @SerializedName("type")
    var type: Int? = null

    @SerializedName("description")
    var description: String? = null

    @SerializedName("amount")
    var amount: Int? = null

    @SerializedName("timestamp")
    var timestamp: String? = null
}