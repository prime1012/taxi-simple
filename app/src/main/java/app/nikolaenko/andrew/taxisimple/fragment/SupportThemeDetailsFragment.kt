package app.nikolaenko.andrew.taxisimple.fragment

import android.app.Activity.RESULT_OK
import android.app.Dialog
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Typeface
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.os.SystemClock
import android.provider.MediaStore
import android.support.v4.content.ContextCompat
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import app.nikolaenko.andrew.taxisimple.R
import app.nikolaenko.andrew.taxisimple.adapter.AnswersAdapter
import app.nikolaenko.andrew.taxisimple.model.*
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.microblink.MicroblinkSDK.getApplicationContext
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.zipWith
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.support_thene_details_fragment.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.jetbrains.anko.find
import org.jetbrains.anko.support.v4.toast
import org.jetbrains.anko.textColor
import java.io.*
import java.text.SimpleDateFormat
import java.util.*


class SupportThemeDetailsFragment : BaseFragment() {

    private var reportInfoDisposable: Disposable? = null
    private var reportDisposable: Disposable? = null
    private var reportId: Int? = null

    private var imgUrl: String? = null

    private lateinit var adatper: AnswersAdapter

    private var mLastClickTime = 0L

    private var isGranted: Boolean = false
    private var file: File? = null

    override fun onStart() {
        super.onStart()
        changeFont()

        reportId = arguments?.getInt(ARG_REPORT_ID)
        getReportInfo(reportId!!)

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.support_thene_details_fragment, container, false)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        leftBtn.setOnClickListener { goBack() }

        adatper = AnswersAdapter {
            url -> showDialogMyImage(url)
        }
        rvChat.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        rvChat.itemAnimator = DefaultItemAnimator()
        rvChat.adapter = adatper
        rvChat.isNestedScrollingEnabled = false

        writeMessage.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (p0?.isNotEmpty()!!){
                    btnSend.visibility = View.VISIBLE
                } else {
                    btnSend.visibility = View.GONE
                }
            }

        })

        btnSend.setOnClickListener {

            if (SystemClock.elapsedRealtime() - mLastClickTime < 1000){
                return@setOnClickListener
            }
            mLastClickTime = SystemClock.elapsedRealtime()

            if ( writeMessage.text.isEmpty()){
                toast("Введите текст сообщения!")
                return@setOnClickListener
            }else{
                if (file != null ) {
                    sendReportWithImage(loginService.value.accessToken, reportId!!, writeMessage.text.toString().trim())
                } else {
                    sendReport(loginService.value.accessToken, reportId!!, writeMessage.text.toString().trim())
                }
            }

        }

        imageLayout.setOnClickListener {
            if (imgUrl!!.isNotEmpty()){
                showDialogMyImage(imgUrl!!)
            }
        }

        addContent.setOnClickListener {
            showDialogAction("Добавить фото", "Галерея", "Камера", "add_photo")
        }
    }

    private fun changeFont() {
        changeFontInTextView(textThemeStatus)
        changeFontInTextView(textTitle)
        changeFontInTextView(textWrite)
        changeFontInTextView(textType)
        changeFontInTextView(imageText)
        changeFontInTextView(textMessage)
        changeFontInTextView(textCurrentDate)
        changeFontInEditTextView(writeMessage)
        changeFontInTextViewBold(textThemeTitle)
        changeFontInTextViewBold(textName)
    }

    private var profile: DriverModel? = null

    private fun getReportInfo(id: Int){

        showProgress(true)

        reportInfoDisposable = taxiService.value.getReportInfo(loginService.value.accessToken, id)
                .zipWith(taxiService.value.getDriverInfo(loginService.value.accessToken)) { t1, t2 ->
                    Pair(t1, t2)
                }
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ t: Pair<ReportInfoResponse, DriverModel> ->

                   if (t.first.report != null){
                       val report = t.first.report!!.reports!!

                       when(report.checked){
                           0-> {
                               textThemeStatus.text = "Обращение открыто"
                               textThemeStatus.textColor = ContextCompat.getColor(context!!, R.color.main_green)
                               sendLayout.visibility = View.VISIBLE
                           }
                           else -> {
                               textThemeStatus.text = "Обращение закрыто"
                               textThemeStatus.textColor = ContextCompat.getColor(context!!, R.color.main_orange)
                               sendLayout.visibility = View.GONE
                           }
                       }
                       textThemeTitle.text = report.topic
                       textCurrentDate.text = report.date


                       val messageList = (t.first.report?.reports?.answer as ArrayList<AnswerModel>?)!!
                       profile = t.second
                       adatper.setList(messageList, profile!!)

                       myMessage.visibility = View.VISIBLE

                       imgUrl = report.patchFile

                       if (t.second.photo != null) {

                           if (t.second.photo!!.isNotEmpty()) {
                               Glide.with(context!!).load(t.second.photo + "/performer?token=${loginService.value.accessToken}").into(userImage)
                           } else {
                               Glide.with(context!!).load(R.drawable.avatar_placeholder).into(userImage)
                           }
                       }

                       textType.text = "Пользователь"
                       textName.text = "${t.second.name} ${t.second.surname}"
                       textMessage.text = t.first.report!!.reports!!.text

                       if (t.first.report?.reports?.patchFile != null) {
                           imageLayout.visibility = View.VISIBLE
                       } else {
                           imageLayout.visibility = View.GONE
                       }

                       imageLayout.setOnClickListener {
                           showDialogMyImage(report.patchFile!!)
                       }

                       scroll.post {
                           scroll.fullScroll(View.FOCUS_DOWN)
                       }

                   }

                    showProgress(false)

                }, { e ->
                    e.printStackTrace()
                    toast(e.localizedMessage)
                    showProgress(false)
                })
    }

    private fun showDialogMyImage(url: String){

        val mDialog = Dialog(context, R.style.CustomDialog)
        mDialog.setCancelable(true)
        mDialog.setContentView(R.layout.preview_fragment_layout)

        val imgPreview = mDialog.find<ImageView>(R.id.imagePreview)
        val progressView = mDialog.find<FrameLayout>(R.id.progressView)

        val requestOptions = RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL)

        if (url.isNotEmpty()) {

            progressView.visibility = View.VISIBLE

            Glide.with(context!!).load(url + "/performer?token=${loginService.value.accessToken}")
                    .apply(requestOptions)
                    .listener(object : RequestListener<Drawable> {

                        override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                            progressView.visibility = View.GONE
                            return false
                        }

                        override fun onResourceReady(resource: Drawable, model: Any, target: Target<Drawable>?, dataSource: DataSource, isFirstResource: Boolean): Boolean {
                            progressView.visibility = View.GONE
                            return false
                        }
                    })
                    .into(imgPreview)
        }

        mDialog.show()
    }

    private fun sendReport(token: String, repId: Int, text: String){

        writeMessage.text.clear()
        writeMessage.clearFocus()
        hideKeyboardFrom(context!!, writeMessage)

        reportDisposable = taxiService.value.answerToReport(token, repId, text)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ t: DataResponse ->

                    if (t.status == "success"){
                        getReportInfo(reportId!!)
                    }

                }, { e ->
                    e.printStackTrace()
                    toast(e.localizedMessage)
                    showProgress(false)
                })
    }

    private fun sendReportWithImage(token: String, repId: Int, text: String) {

        writeMessage.text.clear()
        writeMessage.clearFocus()
        hideKeyboardFrom(context!!, writeMessage)

        val body = MultipartBody.Part.createFormData("token", token)
        val body2 = MultipartBody.Part.createFormData("reports_id", repId.toString())
        val body3 = MultipartBody.Part.createFormData("answer", text)
        val requestFile = RequestBody.create(MediaType.parse("image/png"), file!!)
        val body4 = MultipartBody.Part.createFormData("file", "image.jpg", requestFile)

        reportDisposable = taxiService.value.answerToReportWithImage(body, body2, body3, body4)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ t: DataResponse ->

                    if (t.status == "success"){

                        if (file != null){

                            file?.deleteOnExit()
                            file = null
                        }

                        getReportInfo(reportId!!)
                    }

                }, { e ->
                    e.printStackTrace()
                    toast(e.localizedMessage)
                    showProgress(false)
                })
    }

    private fun showDialogAction(dialogTitle: String, firstBtn: String, secondBtn: String, action: String) {

        val mDialog = Dialog(context, R.style.CustomDialog)
        mDialog.setCancelable(true)
        mDialog.setContentView(R.layout.dialog_with_action)


        val type = Typeface.createFromAsset(context?.assets, "font/opensans_regular.ttf")

        val title = mDialog.find<TextView>(R.id.title)
        title.typeface = type
        title.text = dialogTitle
        val firstButtonText = mDialog?.find<TextView>(R.id.firstButtonText)
        firstButtonText.typeface = type
        firstButtonText.text = firstBtn
        val secondButtonText = mDialog?.find<TextView>(R.id.secondButtonText)
        secondButtonText.typeface = type
        secondButtonText.text = secondBtn
        secondButtonText.textColor = ContextCompat.getColor(context!!, R.color.black)

        firstButtonText.setOnClickListener {
            mDialog.dismiss()

            val intent = Intent(Intent.ACTION_GET_CONTENT)
            intent.type = "image/jpeg"
            try {
                startActivityForResult(intent, GALLERY_REQUEST_CODE)
            } catch (e: ActivityNotFoundException) {
                e.printStackTrace()
            }
        }
        secondButtonText.setOnClickListener {
            mDialog.dismiss()
            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            startActivityForResult(intent, REQUEST_IMAGE)
        }

        mDialog.show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == GALLERY_REQUEST_CODE){

            if (resultCode == RESULT_OK) {
                try {
                    file = File(context!!.filesDir, "img" + Date().time)
                    val cr = mainActivity.contentResolver
                    val `is` = cr.openInputStream(data!!.data!!)
                    file!!.createNewFile()
                    `is`.copyTo(file!!.outputStream())
                    file!!.absolutePath

                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }
        }

        if (requestCode == REQUEST_IMAGE && resultCode == RESULT_OK) {
            val photo = data?.extras?.get("data") as Bitmap
            file = File(bitmapToFile(photo).encodedPath)
        }

    }

    private fun bitmapToFile(bitmap:Bitmap): Uri {
        val wrapper = ContextWrapper(context)

        // Initialize a new file instance to save bitmap object
        var file = wrapper.getDir("Images",Context.MODE_PRIVATE)
        file = File(file,"${UUID.randomUUID()}.jpg")

        try{
            // Compress the bitmap and save in jpg format
            val stream: OutputStream = FileOutputStream(file)
            bitmap.compress(Bitmap.CompressFormat.JPEG,100,stream)
            stream.flush()
            stream.close()
        }catch (e:IOException){
            e.printStackTrace()
        }

        // Return the saved bitmap uri
        return Uri.parse(file.absolutePath)
    }

    override fun onStop() {
        super.onStop()
        reportInfoDisposable?.dispose()
        reportDisposable?.dispose()
    }

    companion object {

        private val GALLERY_REQUEST_CODE = 100
        private val REQUEST_IMAGE = 200


        val ARG_REPORT_ID = "arg_report_id"

        fun newInstance(id: Int): SupportThemeDetailsFragment {

            val args = Bundle()

            args.putInt(ARG_REPORT_ID, id)
            val fragment = SupportThemeDetailsFragment()
            fragment.arguments = args
            return fragment
        }
    }

}
