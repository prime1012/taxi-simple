package app.nikolaenko.andrew.taxisimple.network

import java.io.IOException

import okhttp3.HttpUrl
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response

class AuthOkHttpInterceptor(private val authToken: String) : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val originalRequest = chain.request()
        val originalHttpUrl = originalRequest.url()

        val url = originalHttpUrl.newBuilder()
                .addQueryParameter("Token", authToken)
                .build()

        val newRequest = originalRequest.newBuilder()
                .url(url)
                .build()

        return chain.proceed(newRequest)
    }
}
