package app.nikolaenko.andrew.taxisimple.model

import com.google.gson.annotations.SerializedName

class MessageModel {

    @SerializedName("message")
    var message: String? = null

    @SerializedName("created_at")
    var createdAt: String? = null

    @SerializedName("performer")
    var performer: PerformerModel? = null
}