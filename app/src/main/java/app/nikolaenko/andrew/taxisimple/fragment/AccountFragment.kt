package app.nikolaenko.andrew.taxisimple.fragment

import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import app.nikolaenko.andrew.taxisimple.R
import app.nikolaenko.andrew.taxisimple.adapter.ActivityLinePagerAdapter
import kotlinx.android.synthetic.main.account_fragment.*
import org.jetbrains.anko.textColor


class AccountFragment : BaseFragment() {

    override fun onStart() {
        super.onStart()
        changeFont()

        viewpager.currentItem = 0
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.account_fragment, container, false)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupViewPager(viewpager)

        viewpager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener{
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }

            override fun onPageSelected(position: Int) {
                if (position == 0){
                    accountImg.setImageResource(R.drawable.account_icon)
                    accountText.textColor = ContextCompat.getColor(context!!, R.color.black)

                    autoImg.setImageResource(R.drawable.auto_not_active)
                    autoText.textColor = ContextCompat.getColor(context!!, R.color.main_gray)

                } else {
                    accountImg.setImageResource(R.drawable.account_not_active)
                    accountText.textColor = ContextCompat.getColor(context!!, R.color.main_gray)

                    autoImg.setImageResource(R.drawable.auto_icon)
                    autoText.textColor = ContextCompat.getColor(context!!, R.color.black)

                }
            }

        })

        leftBtn.setOnClickListener { goBack() }

        firstLayout.setOnClickListener {
            viewpager.currentItem = 0
        }

        secondLayout.setOnClickListener {
            viewpager.currentItem = 1
        }
    }

    private fun changeFont(){
       changeFontInTextView(textTitle)
       changeFontInTextView(accountText)
       changeFontInTextView(autoText)
    }

    private fun setupViewPager(viewPager: ViewPager) {

        val adapter = ActivityLinePagerAdapter(childFragmentManager)
        adapter.addFragment(AboutDriverFragment(), getString(R.string.driver_text))
        adapter.addFragment(AboutAutoFragment(), getString(R.string.auto_text))
        viewPager.adapter = adapter
    }
}
