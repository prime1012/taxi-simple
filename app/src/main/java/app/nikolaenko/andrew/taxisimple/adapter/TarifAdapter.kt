package app.nikolaenko.andrew.taxisimple.adapter


import android.graphics.Color
import android.graphics.Typeface
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import app.nikolaenko.andrew.taxisimple.R
import app.nikolaenko.andrew.taxisimple.model.TarifModel
import com.bumptech.glide.Glide
import org.jetbrains.anko.find
import org.jetbrains.anko.textColor

class TarifAdapter (private val callback: ((position: Int) -> Unit)? = null) : RecyclerView.Adapter<TarifAdapter.MyViewHolder>() {

    private var tarifList: List<TarifModel> = listOf()

    var selected_position = 1

    fun setList(list: List<TarifModel>) {
        tarifList = list
        notifyDataSetChanged()
    }

    fun getActiveItem() : Int {
        return selected_position
    }

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var autoImg: ImageView = view.find(R.id.autoImg)
        var tarifTitle: TextView = view.find(R.id.tarifTitle)
        var autoTitle: TextView = view.find(R.id.autoTitle)
        var root: LinearLayout = view.find(R.id.root)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.tarif_item, parent, false)

        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val item = tarifList[position]


        val type = Typeface.createFromAsset(holder.itemView.context!!.assets, "font/opensans_regular.ttf")

        Glide.with(holder.itemView.context).load(item.image).into(holder.autoImg)
        holder.tarifTitle.text = item.title
        holder.autoTitle.text = item.number

        holder.tarifTitle.typeface = type
        holder.autoTitle.typeface = type

        if (selected_position == position) {
            holder.root.setBackgroundColor(ContextCompat.getColor(holder.itemView.context, R.color.lght_gray))
            holder.tarifTitle.textColor = ContextCompat.getColor(holder.itemView.context, R.color.black)
            holder.autoTitle.textColor = ContextCompat.getColor(holder.itemView.context, R.color.black)
        } else {
            holder.root.setBackgroundColor(ContextCompat.getColor(holder.itemView.context, R.color.white))
            holder.tarifTitle.textColor = ContextCompat.getColor(holder.itemView.context, R.color.main_gray)
            holder.autoTitle.textColor = ContextCompat.getColor(holder.itemView.context, R.color.main_gray)
        }

        holder.itemView.setOnClickListener {
            if (holder.adapterPosition == RecyclerView.NO_POSITION) return@setOnClickListener

            notifyItemChanged(selected_position)
            selected_position = holder.adapterPosition
            notifyItemChanged(selected_position)

            callback?.invoke(position)
        }
    }

    override fun getItemCount(): Int {
        return tarifList.size
    }
}
