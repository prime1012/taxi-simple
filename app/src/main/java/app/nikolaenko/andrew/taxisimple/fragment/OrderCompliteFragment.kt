package app.nikolaenko.andrew.taxisimple.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import app.nikolaenko.andrew.taxisimple.R
import app.nikolaenko.andrew.taxisimple.adapter.ServicesAdapter
import app.nikolaenko.andrew.taxisimple.model.DataResponse
import app.nikolaenko.andrew.taxisimple.model.OrderInfoResponse
import app.nikolaenko.andrew.taxisimple.model.ServiceModel
import com.google.android.flexbox.FlexDirection
import com.google.android.flexbox.FlexWrap
import com.google.android.flexbox.FlexboxLayoutManager
import com.google.android.flexbox.JustifyContent
import com.pixplicity.easyprefs.library.Prefs
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.order_completed_fragment.*
import org.jetbrains.anko.support.v4.toast


class OrderCompliteFragment : BaseFragment() {

    private lateinit var adapter: ServicesAdapter
    private var locationDisposable: Disposable? = null
    private var isForBack: Boolean = false
    private var infoDisposable: Disposable? = null
    private var statusDisposable: Disposable? = null
    private var orderId: Int = 0

    override fun onStart() {
        super.onStart()
        changeFont()

        orderId = Prefs.getInt("order_id", 0)
        getOrderInfo(orderId)


//        val list = ArrayList<ServiceModel>()
//        list.clear()
//        list.add(0, ServiceModel("Детское кресло"))
//        list.add(1, ServiceModel("Курящий салон"))
//        list.add(2, ServiceModel("Кондиционер или климат контроль"))
//        adapter.setList(list)

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.order_completed_fragment, container, false)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val layoutManager = FlexboxLayoutManager(context, FlexDirection.ROW, FlexWrap.WRAP)
        layoutManager.justifyContent = JustifyContent.FLEX_START
        rvServices.layoutManager = layoutManager
        adapter = ServicesAdapter()
        rvServices.adapter = adapter
        rvServices.isNestedScrollingEnabled = false

        leftBtn.setOnClickListener {
            isForBack = true

            finishOrder()
        }

        btnSubmit.setOnClickListener {
            isForBack = true

            finishOrder()

        }
    }

    private fun changeFont(){
        changeFontInTextView(textTitle)
        changeFontInTextView(textDate)
        changeFontInTextView(text)
        changeFontInTextView(text3)
        changeFontInTextView(textCurrency)
        changeFontInTextView(textSubmit)
        changeFontInTextView(text2)
        changeFontInTextView(textTo)
        changeFontInTextView(textFrom)

        changeFontInTextViewBold(textName)
        changeFontInTextViewBold(textTime)
        changeFontInTextViewBold(textValue)
    }

    private fun getOrderInfo(id: Int){
        showProgress(true)
//        infoDisposable = taxiService.value.getOrderInfo(loginService.value.accessToken, id)
        infoDisposable = taxiService.value.getOrderInfo(loginService.value.accessToken)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ t: OrderInfoResponse ->

                    if (t.status == "success"){

                        if (t.orderInfo != null) {

                            when(t.orderInfo!!.transportationTariffId){
                                16-> {textType.text = getString(R.string.tarif_ekonom)}
                                17-> {textType.text = getString(R.string.tarif_comfort)}
                                18-> {textType.text = getString(R.string.tarif_busines)}
                                19-> {textType.text = getString(R.string.tarif_vip)}
                            }

                            t.orderInfo!!.fullName?.let {textName.text = it }
                            t.orderInfo!!.resultTripCost?.let { textValue.text = it.toString() }
                            t.orderInfo!!.originText?.let { textFrom.text = it }
                            t.orderInfo!!.destinationText?.let { textTo.text = it }

                            val time = t.orderInfo!!.createdAt.toString()

                            val s = time.substring(0, Math.min(time.length, 10))
                            val strNew = s.replace("-", ".")

                            val strNew2 = time.replace(s+ " ", "")
                            textDate.text = strNew
                            textTime.text = strNew2

                            val paymentType = t.orderInfo!!.paymentType
                            when(paymentType){
                                1->{
                                    text2.text = getString(R.string.payment_type_nal)
                                    imagePayment.setImageResource(R.drawable.nal_icon)
                                }
                                2->{
                                    text2.text = getString(R.string.payment_type_bez_nal)
                                    imagePayment.setImageResource(R.drawable.visa_icon)
                                }
                                3->{
                                    text2.text = getString(R.string.payment_type_virtual)
                                    imagePayment.setImageResource(R.drawable.nal_icon)
                                }
                            }

                            t.orderInfo?.services?.let { adapter.setList(it) }

                        }

                    }

                    showProgress(false)
                }, { e ->
                    e.printStackTrace()
                    showProgress(false)
                    toast(e.localizedMessage)
                })

    }

    override fun onStop() {
        super.onStop()
        infoDisposable?.dispose()
        statusDisposable?.dispose()
    }

//    private fun updateLocation(){
//
//        val lat = Prefs.getDouble("my_latitude", 0.0)
//        val lon = Prefs.getDouble("my_longitude", 0.0)
//
//        locationDisposable = taxiService.value.updateLocation(loginService.value.accessToken, lat.toString(), lon.toString())
//                .subscribeOn(Schedulers.newThread())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe({ t: DataResponse ->
//
//                    Prefs.remove("order_taked")
//                    Prefs.remove("order_id")
//
//                    mainActivity.hardReplaceFragment(OrdersFragment())
//
//                }, { e ->
//                    e.printStackTrace()
//                    toast(e.localizedMessage)
//                })
//    }

    private fun finishOrder(){

        val lat = Prefs.getDouble("my_latitude", 0.0)
        val lon = Prefs.getDouble("my_longitude", 0.0)

        statusDisposable = taxiService.value.setPerformerOrderStatus(loginService.value.accessToken, orderId, 5, lat.toString(), lon.toString())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ t: DataResponse ->

                    if (t.status == "success") {

                        taxiService.value.finishOrder(loginService.value.accessToken, orderId)
                                .subscribeOn(Schedulers.newThread())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe({ t: DataResponse ->

                                    Prefs.remove("order_taked")
                                    Prefs.remove("order_id")
                                    mainActivity.hardReplaceFragment(OrdersFragment())

                                }, { e ->
                                    e.printStackTrace()
                                    toast(e.localizedMessage)
                                })
                    }
                }, { e ->
                    e.printStackTrace()
                    toast(e.localizedMessage)
                })
    }
}
