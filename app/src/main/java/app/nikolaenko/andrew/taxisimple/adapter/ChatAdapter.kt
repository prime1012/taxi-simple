package app.nikolaenko.andrew.taxisimple.adapter

import android.graphics.Typeface
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.TextView
import app.nikolaenko.andrew.taxisimple.R
import app.nikolaenko.andrew.taxisimple.model.MessageModel
import org.jetbrains.anko.find
import java.util.*

class ChatAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var list: MutableList<MessageModel> = ArrayList()
    private var myId: Int? = null

    fun setList(list: List<MessageModel>, myId: Int) {
        this.list = list.toMutableList()
        this.myId = myId
        notifyDataSetChanged()
    }

    class TextMessageHolder constructor(view: View) : RecyclerView.ViewHolder(view) {
        internal var textAuto: TextView = view.find(R.id.textAuto)
        internal var textName: TextView = view.find(R.id.textName)
        internal var textDate: TextView = view.find(R.id.textDate)
        internal var textMessage: TextView = view.find(R.id.textMessage)
    }

    class MyTextMessageHolder constructor(view: View) : RecyclerView.ViewHolder(view) {
        internal var textAuto: TextView = view.find(R.id.textAuto)
        internal var textName: TextView = view.find(R.id.textName)
        internal var textDate: TextView = view.find(R.id.textDate)
        internal var textMessage: TextView = view.find(R.id.textMessage)
    }

    class NoCrashHolder constructor(view: View) : RecyclerView.ViewHolder(view)

    override fun getItemViewType(position: Int): Int {

        val message = list[position]

        if (message.performer!!.id == myId){
            return MY_TEXT
        } else {
            return TEXT
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val mInflater = LayoutInflater.from(parent.context)
        when (viewType) {

            TEXT -> {
                val textGroup = mInflater.inflate(R.layout.user_message_item, parent, false) as ViewGroup
                return TextMessageHolder(textGroup)
            }

            MY_TEXT -> {
                val myTextGroup = mInflater.inflate(R.layout.my_message_item, parent, false) as ViewGroup
                return MyTextMessageHolder(myTextGroup)
            }

            else -> return NoCrashHolder(FrameLayout(parent.context))
        }
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val chat = list[position]

        val type = Typeface.createFromAsset(holder.itemView.context!!.assets, "font/opensans_regular.ttf")
        val typeBold = Typeface.createFromAsset(holder.itemView.context!!.assets, "font/opensans_bold.ttf")

        when (holder.itemViewType) {

            TEXT -> {
                val textHolder = holder as TextMessageHolder
                textHolder.textAuto.text = "Номер машины"
                textHolder.textDate.text = chat.createdAt
                val name = chat.performer?.fullName?.name + " " + chat.performer?.fullName?.surname
                textHolder.textName.text = name
                textHolder.textMessage.text = chat.message

                textHolder.textAuto.typeface = type
                textHolder.textDate.typeface = type
                textHolder.textMessage.typeface = type
                textHolder.textName.typeface = typeBold
            }

            MY_TEXT -> {
                val myTextHolder = holder as MyTextMessageHolder
                myTextHolder.textAuto.text = "Номер машины"
                myTextHolder.textDate.text = chat.createdAt
                val name = chat.performer?.fullName?.name + " " + chat.performer?.fullName?.surname
                myTextHolder.textName.text = name
                myTextHolder.textMessage.text = chat.message

                myTextHolder.textAuto.typeface = type
                myTextHolder.textDate.typeface = type
                myTextHolder.textMessage.typeface = type
                myTextHolder.textName.typeface = typeBold
            }
        }
    }

    companion object {

        private val TEXT = 1
        private val MY_TEXT = 2
    }
}