package app.nikolaenko.andrew.taxisimple.fragment

import android.app.Activity
import android.content.Context
import android.graphics.Typeface
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView
import app.nikolaenko.andrew.taxisimple.R
import app.nikolaenko.andrew.taxisimple.activity.MainActivity
import app.nikolaenko.andrew.taxisimple.network.LoginService
import app.nikolaenko.andrew.taxisimple.network.TaxiPublicService
import app.nikolaenko.andrew.taxisimple.network.TaxiService
import com.github.salomonbrys.kodein.LazyKodein
import com.github.salomonbrys.kodein.android.appKodein
import com.github.salomonbrys.kodein.instance


abstract class BaseFragment : Fragment() {

    protected val kodein = LazyKodein(appKodein)

    protected val taxiService = kodein.instance<TaxiService>()
    protected val loginService = kodein.instance<LoginService>()
    protected val taxiPulicService = kodein.instance<TaxiPublicService>()

    protected lateinit var mainActivity: MainActivity

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mainActivity = activity as MainActivity
    }


    fun showProgress(show: Boolean) {
        mainActivity.progressView!!.visibility = if (show) View.VISIBLE else View.GONE
    }

    fun showTabs(show: Boolean) {
        mainActivity.bottomTabs!!.visibility = if (show) View.VISIBLE else View.GONE
    }

    fun replaceFragment(fragment: Fragment, addToBackStack: Boolean = true, tag: String? = null) {
        val ft = mainActivity.supportFragmentManager.beginTransaction()
        if(tag == null)
            ft.replace(R.id.placeholder, fragment)
        else
            ft.replace(R.id.placeholder, fragment, tag)
        if (addToBackStack) {
            ft.addToBackStack(null)
        }

        ft.commit()
    }

    fun addFragment(fragment: Fragment, addToBackStack: Boolean = true, tag: String? = null) {
        val ft = mainActivity.supportFragmentManager.beginTransaction()
        if(tag == null)
            ft.add(R.id.placeholder, fragment)
        else
            ft.add(R.id.placeholder, fragment, tag)
        if (addToBackStack) {
            ft.addToBackStack(null)
        }

        ft.commit()
    }

    fun hideKeyboard() {
        val imm = mainActivity.applicationContext.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        if (imm.isActive) {
            view?.let {
                imm.hideSoftInputFromWindow(it.windowToken, 0)
            }
        }
    }

    fun goBack(){

        if (mainActivity.supportFragmentManager.backStackEntryCount > 0) {
            mainActivity.supportFragmentManager.popBackStackImmediate()
        }

    }

    fun changeFontInTextView(view: TextView){
        val type = Typeface.createFromAsset(context?.assets, "font/opensans_regular.ttf")
        view.typeface = type
    }

    fun changeFontInTextViewBold(view: TextView){
        val type = Typeface.createFromAsset(context?.assets, "font/opensans_bold.ttf")
        view.typeface = type
    }

    fun changeFontInEditTextView(view: EditText){
        val type = Typeface.createFromAsset(context?.assets, "font/opensans_regular.ttf")
        view.typeface = type
    }

    fun changeFontInEditTextViewBold(view: EditText){
        val type = Typeface.createFromAsset(context?.assets, "font/opensans_bold.ttf")
        view.typeface = type
    }

    fun hideKeyboardFrom(context: Context, view: View) {
        val imm = context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }
}
