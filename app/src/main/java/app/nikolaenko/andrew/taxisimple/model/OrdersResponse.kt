package app.nikolaenko.andrew.taxisimple.model

import com.google.gson.annotations.SerializedName

class OrdersResponse {

    @SerializedName("status")
    var status: String? = null

    @SerializedName("completedOrders")
    var completedOrders: List<RideModel>? = null
}