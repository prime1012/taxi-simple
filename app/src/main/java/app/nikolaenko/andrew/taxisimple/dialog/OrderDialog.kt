package app.nikolaenko.andrew.taxisimple.dialog


import android.graphics.Typeface
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.DialogFragment
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import app.nikolaenko.andrew.taxisimple.R
import app.nikolaenko.andrew.taxisimple.fragment.OrderCompliteFragment
import app.nikolaenko.andrew.taxisimple.model.DataResponse
import app.nikolaenko.andrew.taxisimple.model.OrderInfoModel
import app.nikolaenko.andrew.taxisimple.model.OrderInfoResponse
import app.nikolaenko.andrew.taxisimple.network.LoginService
import app.nikolaenko.andrew.taxisimple.network.TaxiService
import com.github.salomonbrys.kodein.LazyKodein
import com.github.salomonbrys.kodein.android.appKodein
import com.github.salomonbrys.kodein.instance
import com.google.gson.Gson
import com.pixplicity.easyprefs.library.Prefs
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.taked_order_dialog.*
import org.jetbrains.anko.support.v4.toast
import android.R.attr.phoneNumber
import android.content.Intent
import android.net.Uri


class OrderDialog : DialogFragment(){

    private var orderId: Int = 0
    private var userNumder: String? = null


    protected val kodein = LazyKodein(appKodein)
    protected val taxiService = kodein.instance<TaxiService>()
    protected val loginService = kodein.instance<LoginService>()

    private var statusDisposable: Disposable? = null
    private var orderDisposable: Disposable? = null

    override fun onStart() {
        super.onStart()

        orderId = Prefs.getInt("order_id", 0)

        getOrderInfo(orderId)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.taked_order_dialog, container, false)

        if (dialog != null && dialog.window != null) {
            dialog.window.requestFeature(Window.FEATURE_NO_TITLE)
            isCancelable = false
            dialog.window.setBackgroundDrawableResource(android.R.color.transparent)
            dialog.window.setDimAmount(0.2f)
        }

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        val type = Typeface.createFromAsset(context?.assets, "font/opensans_regular.ttf")
        val typeBold = Typeface.createFromAsset(context?.assets, "font/opensans_bold.ttf")

        text.typeface = typeBold

        textFrom.typeface = type
        textTo.typeface = type
        textName.typeface = type
        text2.typeface = type
        textTime.typeface = type
        text3.typeface = type
        textDate.typeface = type
        textArraive.typeface = type
        textArraive2.typeface = type
        textArraive3.typeface = type

        prView.indeterminateDrawable.setColorFilter(ContextCompat.getColor(context!!, R.color.colorPrimary), android.graphics.PorterDuff.Mode.SRC_ATOP)

        layoutCall.setOnClickListener {
            val intent = Intent(Intent.ACTION_DIAL)
            intent.data = Uri.parse("tel:$userNumder")
            if (intent.resolveActivity(context!!.packageManager) != null) {
                startActivity(intent)
            }
        }

        btnHere.setOnClickListener {
            changeStatusOfOrder(3)
        }

        btnStart.setOnClickListener {
            changeStatusOfOrder(4)
        }

        btnFinish.setOnClickListener {
            changeStatusOfOrder(5)
        }

    }

    private fun getOrderInfo(id: Int){

//        orderDisposable = taxiService.value.getOrderInfo(loginService.value.accessToken, id)
        orderDisposable = taxiService.value.getOrderInfo(loginService.value.accessToken)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ t: OrderInfoResponse ->

                    if (t.status == "success"){
                        if (t.orderInfo != null){
                            userNumder = t.orderInfo!!.phoneNumber
                            t.orderInfo!!.originText?.let { textFrom.text = it }
                            t.orderInfo!!.destinationText?.let { textTo.text = it }
                            t.orderInfo!!.fullName?.let { textName.text = it }

                            val time = t.orderInfo!!.createdAt.toString()

                            val s = time.substring(0, Math.min(time.length, 10))
                            val strNew = s.replace("-", ".")

                            val strNew2 = time.replace("$s ", "")
                            textDate.text = "$strNew год"
                            textTime.text = strNew2

                            val paymentType = t.orderInfo!!.paymentType
                            when(paymentType){
                                1->{
                                    imagePayment.setImageResource(R.drawable.nal_icon)
                                }
                                2->{
                                    imagePayment.setImageResource(R.drawable.visa_icon)
                                }
                                3->{
                                    imagePayment.setImageResource(R.drawable.nal_icon)
                                }
                            }

                        }
                    }

                }, { e ->
                    e.printStackTrace()
                    toast(e.localizedMessage)
                })
    }

    private fun changeStatusOfOrder(status: Int){

        val lat = Prefs.getDouble("my_latitude", 0.0)
        val lon = Prefs.getDouble("my_longitude", 0.0)

        statusDisposable = taxiService.value.setPerformerOrderStatus(loginService.value.accessToken, orderId, status, lat.toString(), lon.toString())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ t: DataResponse ->
                    if (t.status == "success") {

                        when(status){
                            3 -> {
                                orderProgress.visibility = View.VISIBLE

                                firstStepLayout.visibility = View.GONE

                                val handler = Handler()
                                handler.postDelayed({
                                    orderProgress.visibility = View.GONE
                                    btnHere.visibility = View.GONE
                                    btnStart.visibility = View.VISIBLE
                                    firstStepLayout.visibility = View.VISIBLE

                                }, 1000)
                            }
                            4 -> {
                                orderProgress.visibility = View.VISIBLE

                                firstStepLayout.visibility = View.GONE

                                val handler = Handler()
                                handler.postDelayed({
                                    orderProgress.visibility = View.GONE
                                    btnHere.visibility = View.GONE
                                    btnStart.visibility = View.GONE
                                    btnFinish.visibility = View.VISIBLE
                                    firstStepLayout.visibility = View.VISIBLE

                                }, 1000)
                            }
                            5 -> {
                                dismiss()

                                val ft = activity?.supportFragmentManager?.beginTransaction()
                                ft?.replace(R.id.placeholder, OrderCompliteFragment(), "order_complite")
                                ft?.addToBackStack(null)
                                ft?.commit()
                            }
                        }

                    }

                }, { e ->
                    e.printStackTrace()
                    toast(e.localizedMessage)
                })
    }

    override fun onStop() {
        super.onStop()
        statusDisposable?.dispose()
        orderDisposable?.dispose()
    }

    companion object {
        val ARG_STATUS = "arg_order_id"
        val ARG_FROM = "arg_order_from"
        val ARG_TO = "arg_order_to"
        val ARG_NAME = "arg_order_name"
        val ARG_DATE = "arg_order_date"
        val ARG_TYPE = "arg_order_payment_type"

        fun newInstance(orderId: Int, from: String, to: String, name: String, date: String, type: Int): OrderDialog {

            val args = Bundle()
            args.putInt(ARG_STATUS, orderId)
            args.putString(ARG_FROM, from)
            args.putString(ARG_TO, to)
            args.putString(ARG_NAME, name)
            args.putString(ARG_DATE, date)
            args.putInt(ARG_TYPE, type)
            val fragment = OrderDialog()
            fragment.arguments = args
            return fragment
        }
    }
}
