package app.nikolaenko.andrew.taxisimple.model

import com.google.gson.annotations.SerializedName

class OrderInfoModel {

    @SerializedName("id")
    var id: Int?= null

    @SerializedName("client_id")
    var clientId: Int?= null

    @SerializedName("created_at")
    var createdAt: String? = null

    @SerializedName("updated_at")
    var updatedAt: String? = null

    @SerializedName("transportation_tariff_id")
    var transportationTariffId: Int?= null

    @SerializedName("transportation_tariff_name")
    var transportationTariffName: String? = null

    @SerializedName("origin_latitude")
    var originLatitude: String? = null

    @SerializedName("origin_longitude")
    var originLongitude: String? = null

    @SerializedName("destination_latitude")
    var destinationLatitude: String? = null

    @SerializedName("destination_longitude")
    var destinationLongitude: String? = null


    @SerializedName("service_ids")
    var serviceIds: String? = null

    @SerializedName("promo_code_id")
    var promoCodeId: Int?= null

    @SerializedName("payment_type")
    var paymentType: Int?= null

    @SerializedName("order_source")
    var iorderSourced: Int?= null

    @SerializedName("card_id")
    var cardId: Int?= null

    @SerializedName("favorite_performer_id")
    var favoritePerformerId: Int?= null

    @SerializedName("counting_type")
    var countingType: Int?= null

    @SerializedName("status")
    var status: Int?= null

    @SerializedName("origin_text")
    var originText: String? = null

    @SerializedName("destination_text")
    var destinationText: String? = null

    @SerializedName("full_name")
    var fullName: String? = null

    @SerializedName("phone_number")
    var phoneNumber: String? = null

    @SerializedName("trip_time")
    var tripTime: String? = null

    @SerializedName("trip_distance")
    var tripDistance: String? = null

    @SerializedName("transmit_cost")
    var transmitCost: Int?= null

    @SerializedName("cost_in_zone")
    var costInZone: Int?= null

    @SerializedName("outdoor_zone_cost")
    var outdoorZoneCost: Int?= null

    @SerializedName("sub_zones_cost")
    var subZonesCost: Int?= null

    @SerializedName("perform_cost")
    var performCost: Int?= null

    @SerializedName("services_cost")
    var servicesCost: Int?= null

    @SerializedName("min_cost")
    var minCost: Int?= null

    @SerializedName("discount_by_promo")
    var discountByPromo: Int?= null

    @SerializedName("result_trip_cost")
    var resultTripCost: Int?= null

    @SerializedName("trip_cost_with_discount")
    var tripCostWithDiscount: Int?= null

    @SerializedName("trip_cost")
    var tripCost: Int?= null

    @SerializedName("services")
    var services: List<addServiceModel>? = null
}