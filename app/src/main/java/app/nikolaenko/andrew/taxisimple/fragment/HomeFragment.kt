package app.nikolaenko.andrew.taxisimple.fragment

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Base64
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import app.nikolaenko.andrew.taxisimple.BuildConfig
import app.nikolaenko.andrew.taxisimple.R
import app.nikolaenko.andrew.taxisimple.model.LoginResponse
import app.nikolaenko.andrew.taxisimple.network.LoginService
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.microblink.entities.recognizers.RecognizerBundle
import com.microblink.entities.recognizers.blinkbarcode.barcode.BarcodeRecognizer
import com.microblink.uisettings.ActivityRunner
import com.microblink.uisettings.BarcodeUISettings
import com.pixplicity.easyprefs.library.Prefs
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.start_fragment.*
import org.jetbrains.anko.support.v4.toast


class HomeFragment : BaseFragment() {

    private val MY_REQUEST_CODE = 1337
    private var isAccepted: Boolean = false
    private var phoneNumber: String? = null
    private var scannedSerieNumber: String? = null
    private var mBarcodeRecognizer: BarcodeRecognizer? = null
    private var mRecognizerBundle: RecognizerBundle? = null

    override fun onStart() {
        super.onStart()

        changeFont()
        getCameraPermission()
        phoneNumber = arguments?.getString(ARG_FOR_NUMBER)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.start_fragment, container, false)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mBarcodeRecognizer = BarcodeRecognizer()
        mBarcodeRecognizer?.setScanPdf417(true)
        mRecognizerBundle = RecognizerBundle(mBarcodeRecognizer)

        toobarLeft.setOnClickListener {
            goBack()
        }

        btnScan.setOnClickListener {
            if (isAccepted) {

//                replaceFragment(ScanFragment(), true)
                onScanButtonClick()
            } else {
                toast("Разрешите использовать камеру!")
            }
        }

        text2.setOnClickListener {
            if (BuildConfig.DEBUG){
                showProgress(true)
                val handler = Handler()
                handler.postDelayed({
                    showProgress(false)
                    mainActivity.hardReplaceFragment(DriverMainFragment())
                }, 2000)
            }
        }
    }

    private fun getCameraPermission(){

        Dexter.withActivity(mainActivity)
                .withPermissions(
                        Manifest.permission.CAMERA, Manifest.permission.ACCESS_COARSE_LOCATION,  Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(object : MultiplePermissionsListener {

                    override fun onPermissionRationaleShouldBeShown(permissions: MutableList<com.karumi.dexter.listener.PermissionRequest>?, token: PermissionToken?) {
                        token?.continuePermissionRequest()
                    }

                    override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                        if (report?.areAllPermissionsGranted()!!){
                            isAccepted = true
                        }
                    }
                }).check()
    }

    private fun onScanButtonClick() {
        // start default barcode scanning activity
        val uiSettings = BarcodeUISettings(mRecognizerBundle!!)
        uiSettings.setBeepSoundResourceID(R.raw.beep)
        uiSettings.setShowDialogAfterScan(false)
        ActivityRunner.startActivityForResult(this, MY_REQUEST_CODE, uiSettings)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == MY_REQUEST_CODE && resultCode == Activity.RESULT_OK && data != null) {

            mRecognizerBundle?.loadFromIntent(data)
            val result = mBarcodeRecognizer?.result

            val datasd = Base64.decode(result?.stringData, Base64.DEFAULT)
            val text = String(datasd, charset("UTF-8"))
            scannedSerieNumber = text.replace("\\|.*".toRegex(), "")

            if (phoneNumber != null && scannedSerieNumber != null){

                showProgress(true)

                val phoneNumb = phoneNumber!!.replace("(", "")
                val phoneNumb2 = phoneNumb.replace(")", "")
                val phoneNumb3 = phoneNumb2.replace("+", "")
                val phoneNumb4 = phoneNumb3.replace("-", "")
                val phoneNumb5 = phoneNumb4.replace(" ", "")

                taxiPulicService.value.signInObservable(phoneNumb5, scannedSerieNumber!!)
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({ t: LoginResponse ->

                            if (t.token != null){
//                                Prefs.putString("access_token", t.token)
                                Prefs.putString(LoginService.ACCESS_TOKEN_KEY, t.token)
                                mainActivity.hardReplaceFragment(DriverMainFragment())
                            }

                            showProgress(false)

                        }, { e ->
                            e.printStackTrace()
                            toast(e.localizedMessage)
                            showProgress(false)
                        })
            }
        }
    }

    private fun changeFont(){
        changeFontInTextView(text)
        changeFontInTextView(text2)
        changeFontInTextView(textScan)
        changeFontInTextView(textBack)
    }

    companion object {

        val ARG_FOR_NUMBER = "arg_phone_number"

        fun newInstance(number: String): HomeFragment {

            val args = Bundle()

            args.putString(ARG_FOR_NUMBER, number)

            val fragment = HomeFragment()
            fragment.arguments = args
            return fragment
        }
    }
}
