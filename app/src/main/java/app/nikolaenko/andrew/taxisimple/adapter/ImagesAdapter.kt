package app.nikolaenko.andrew.taxisimple.adapter


import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import app.nikolaenko.andrew.taxisimple.R
import app.nikolaenko.andrew.taxisimple.model.Image
import com.bumptech.glide.Glide
import org.jetbrains.anko.find

class ImagesAdapter : RecyclerView.Adapter<ImagesAdapter.MyViewHolder>() {

    private var imageList: List<Image> = listOf()

    fun setList(list: List<Image>) {
        imageList = list
        notifyDataSetChanged()
    }

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var imageView: ImageView = view.find(R.id.image)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.images_item, parent, false)

        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val imgItem = imageList[position]

        Glide.with(holder.itemView.context).load(imgItem.image).into(holder.imageView)

    }

    override fun getItemCount(): Int {
        return imageList.size
    }
}
