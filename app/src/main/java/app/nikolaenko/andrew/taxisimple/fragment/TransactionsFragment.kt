package app.nikolaenko.andrew.taxisimple.fragment

import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import app.nikolaenko.andrew.taxisimple.R
import app.nikolaenko.andrew.taxisimple.adapter.TransactionsAdapter
import app.nikolaenko.andrew.taxisimple.model.TransactionsResponse
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.transactions_fragment.*
import org.jetbrains.anko.support.v4.toast


class TransactionsFragment : BaseFragment() {

    private lateinit var transactionsAdapter: TransactionsAdapter
    private var disposableTransactions: Disposable? = null



    override fun onStart() {
        super.onStart()

        getLastTransactions()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.transactions_fragment, container, false)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        transactionsAdapter = TransactionsAdapter()
        rvTransactions.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        rvTransactions.itemAnimator = DefaultItemAnimator()
        rvTransactions.adapter = transactionsAdapter
        rvTransactions.isNestedScrollingEnabled = false
    }

    private fun getLastTransactions(){

        showProgress(true)

        disposableTransactions = taxiService.value.getTransactions(loginService.value.accessToken)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ t: TransactionsResponse ->

                    if (t.status == "success"){

                        transactionsAdapter.setList(t.transactions!!)
                    }

                    showProgress(false)
                }, { e ->
                    e.printStackTrace()
                    toast(e.localizedMessage)
                    showProgress(false)
                })
    }

    override fun onStop() {
        super.onStop()
        disposableTransactions?.dispose()
    }
}
