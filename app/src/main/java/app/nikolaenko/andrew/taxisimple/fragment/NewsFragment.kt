package app.nikolaenko.andrew.taxisimple.fragment

import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import app.nikolaenko.andrew.taxisimple.R
import app.nikolaenko.andrew.taxisimple.adapter.NewsAdapter
import app.nikolaenko.andrew.taxisimple.model.NewsModel
import kotlinx.android.synthetic.main.news_fragment.*


class NewsFragment : BaseFragment() {

    private lateinit var newsAdapter: NewsAdapter

    override fun onStart() {
        super.onStart()
        changeFont()

        val list = ArrayList<NewsModel>()
        list.clear()
        list.add(0, NewsModel("Менеджер", "12:45", "12.10.2018 год", "Название новости", getString(R.string.sample_text)))
        list.add(1, NewsModel("Менеджер", "12:45", "12.10.2018 год", "Название новости", getString(R.string.sample_text)))
        list.add(2, NewsModel("Менеджер", "12:45", "12.10.2018 год", "Название новости", getString(R.string.sample_text)))
        list.add(3, NewsModel("Менеджер", "12:45", "12.10.2018 год", "Название новости", getString(R.string.sample_text)))
        list.add(4, NewsModel("Менеджер", "12:45", "12.10.2018 год", "Название новости", getString(R.string.sample_text)))
        list.add(5, NewsModel("Менеджер", "12:45", "12.10.2018 год", "Название новости", getString(R.string.sample_text)))
        newsAdapter.setList(list)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.news_fragment, container, false)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        newsAdapter = NewsAdapter{
            fromWho, time, date, title, text ->
            replaceFragment(NewsDescriptionFragment.newInstance(fromWho!!, time!!, date!!, title!!, text!!), true)
        }
        rvNews.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        rvNews.itemAnimator = DefaultItemAnimator()
        rvNews.adapter = newsAdapter
        rvNews.isNestedScrollingEnabled = false

        leftBtn.setOnClickListener { goBack() }
    }

    private fun changeFont(){
        changeFontInTextView(textTitle)
    }
}
