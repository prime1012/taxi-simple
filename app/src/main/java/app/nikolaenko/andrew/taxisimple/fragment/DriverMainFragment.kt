package app.nikolaenko.andrew.taxisimple.fragment

import android.Manifest
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import app.nikolaenko.andrew.taxisimple.R
import app.nikolaenko.andrew.taxisimple.model.DataResponse
import app.nikolaenko.andrew.taxisimple.model.DriverModel
import com.bumptech.glide.Glide
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.pixplicity.easyprefs.library.Prefs
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.driver_main_fragment.*
import org.jetbrains.anko.support.v4.toast

class DriverMainFragment : BaseFragment() {

    private var infoDisposable: Disposable? = null
    private var firebaseDisposable: Disposable? = null

    private var userId: Int? = null

    private var isGranted: Boolean = false

    override fun onStart() {
        super.onStart()
        changeFont()

        getDriverInfo()

        if (!isGranted) {
            getLocationPermissions()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.driver_main_fragment, container, false)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btnAccount.setOnClickListener {
            replaceFragment(AccountFragment(), true)
        }

        btnOrders.setOnClickListener {
            if (isGranted){
                mainActivity.hardReplaceFragment(OrdersFragment())
            } else {
                getLocationPermissions()
                return@setOnClickListener
            }
        }

        btnMoney.setOnClickListener {
            replaceFragment(MoneyFragment.newInstance(false), true)
        }

        btnNews.setOnClickListener {
            showProgress(true)
            val handler = Handler()
            handler.postDelayed({
                showProgress(false)
                replaceFragment(NewsFragment(), true)
            }, 1000)
        }

        btnSupport.setOnClickListener {
            replaceFragment( SupportFragment(), true)
        }

        btnChat.setOnClickListener {
            replaceFragment(ChatFragment.newInstance(userId!!), true)
        }

        btnSettings.setOnClickListener {
            replaceFragment(AccountSettingsFragment(), true)
        }

        btnLogout.setOnClickListener {
            showProgress(true)
            loginService.value.logout()
            toast("Вы вышли из аккаунта!")
            mainActivity.hardReplaceFragment(PhoneFragment())
        }
    }

    private fun getDriverInfo(){

        showProgress(true)
        infoDisposable = taxiService.value.getDriverInfo(loginService.value.accessToken)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ t: DriverModel ->

                    userId = t.id

                    userName.text = "${t.name} ${t.middleName} \n${t.surname}"

                    if (t.photo != null) {

                        if (t.photo!!.isNotEmpty()) {
                            Glide.with(context!!).load(t.photo + "/performer?token=${loginService.value.accessToken}").into(userPhoto)
                        } else {
                            Glide.with(context!!).load(R.drawable.avatar_placeholder).into(userPhoto)
                        }
                    }

                    t.rating?.let { starCount.text = it }

                    val gson = Gson()
                    val json = gson.toJson(t)
                    Prefs.putString("driver_info", json)

                    sendFirebaseToken()

                    showProgress(false)

                }, { e ->
                    e.printStackTrace()
                    toast(e.localizedMessage)
                    showProgress(false)
                })

    }

    private fun getLocationPermissions(){

        Dexter.withActivity(mainActivity)
                .withPermissions(
                        Manifest.permission.CAMERA, Manifest.permission.ACCESS_COARSE_LOCATION,  Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(object : MultiplePermissionsListener {

                    override fun onPermissionRationaleShouldBeShown(permissions: MutableList<com.karumi.dexter.listener.PermissionRequest>?, token: PermissionToken?) {
                        token?.continuePermissionRequest()
                    }

                    override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                        if (report?.areAllPermissionsGranted()!!){
                            isGranted = true
                        }
                    }
                }).check()
    }

    private fun changeFont(){
        changeFontInTextView(starCount)
        changeFontInTextView(text)
        changeFontInTextView(accountText)
        changeFontInTextView(ordersText)
        changeFontInTextView(paymentsText)
        changeFontInTextView(newsText)
        changeFontInTextView(supportText)
        changeFontInTextView(chatText)

        changeFontInTextViewBold(userName)
        changeFontInTextViewBold(textSettings)
    }

    private fun sendFirebaseToken(){

        val myToken = loginService.value.accessToken
        val fireToken = FirebaseInstanceId.getInstance().token

        firebaseDisposable?.dispose()
        firebaseDisposable = taxiService.value.sendFirebaseToken(myToken, fireToken!!)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ t: DataResponse ->

                }, { e ->
                    e.printStackTrace()
                    toast(e.localizedMessage)
                })

    }

    override fun onStop() {
        super.onStop()
        infoDisposable?.dispose()
        firebaseDisposable?.dispose()
    }
}
