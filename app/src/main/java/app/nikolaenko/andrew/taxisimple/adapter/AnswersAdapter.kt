package app.nikolaenko.andrew.taxisimple.adapter

import android.graphics.Typeface
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import app.nikolaenko.andrew.taxisimple.R
import app.nikolaenko.andrew.taxisimple.model.*
import app.nikolaenko.andrew.taxisimple.network.LoginService
import com.bumptech.glide.Glide
import com.github.salomonbrys.kodein.LazyKodein
import com.github.salomonbrys.kodein.android.appKodein
import com.github.salomonbrys.kodein.instance
import org.jetbrains.anko.find
import java.util.*

class AnswersAdapter(private val callback: ((image: String) -> Unit)? = null) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var list: MutableList<AnswerModel> = ArrayList()
    var userProfile: DriverModel? = null

    fun setList(list: List<AnswerModel>, userProfile: DriverModel) {
        this.list = list.toMutableList()
        this.userProfile = userProfile
        notifyDataSetChanged()
    }

    class TextMessageHolder constructor(view: View) : RecyclerView.ViewHolder(view) {
        internal var textTp: TextView = view.find(R.id.textType)
        internal var textName: TextView = view.find(R.id.textName)
        internal var textMessage: TextView = view.find(R.id.textMessage)
        internal var imageLayout: LinearLayout = view.find(R.id.imageLayout)
    }

    class MyTextMessageHolder constructor(view: View) : RecyclerView.ViewHolder(view) {
        internal var textTp: TextView = view.find(R.id.textType)
        internal var textName: TextView = view.find(R.id.textName)
        internal var textMessage: TextView = view.find(R.id.textMessage)
        internal var imageLayout: LinearLayout = view.find(R.id.imageLayout)
        internal var userImage: ImageView = view.find(R.id.userImage)
    }

    class NoCrashHolder constructor(view: View) : RecyclerView.ViewHolder(view)

    override fun getItemViewType(position: Int): Int {
        val item = list[position]

        if (item.accountType == 2) {
            return MY_TEXT
        } else {
            return TEXT
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val mInflater = LayoutInflater.from(parent.context)
        when (viewType) {

            TEXT -> {
                val textGroup = mInflater.inflate(R.layout.support_user_item, parent, false) as ViewGroup
                return TextMessageHolder(textGroup)
            }

            MY_TEXT -> {
                val textGroup = mInflater.inflate(R.layout.support_my_item, parent, false) as ViewGroup
                return MyTextMessageHolder(textGroup)
            }

            else -> return NoCrashHolder(FrameLayout(parent.context))
        }
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val report = list[position]

        val type = Typeface.createFromAsset(holder.itemView.context!!.assets, "font/opensans_regular.ttf")
        val typeBold = Typeface.createFromAsset(holder.itemView.context!!.assets, "font/opensans_bold.ttf")

        val loginService: LoginService = LazyKodein(holder.itemView.context!!.applicationContext.appKodein).instance<LoginService>().value

        when (holder.itemViewType) {

            TEXT -> {
                val textHolder = holder as TextMessageHolder
                textHolder.textTp.text = "Диспетчер"
                textHolder.textName.text = report.userInfo!!.name
                textHolder.textMessage.text = report.answer

                textHolder.textTp.typeface = type
                textHolder.textMessage.typeface = type
                textHolder.textName.typeface = typeBold

                if (report.pathFile != null && report.pathFile!!.isNotEmpty()){
                    holder.imageLayout.visibility = View.VISIBLE
                } else {
                    holder.imageLayout.visibility = View.GONE
                }

                holder.imageLayout.setOnClickListener {
                    callback?.invoke(report.pathFile!!)
                }
            }

            MY_TEXT -> {
                val textHolder = holder as MyTextMessageHolder
                textHolder.textTp.text = "Пользователь"
                textHolder.textName.text = "${userProfile!!.name} ${userProfile!!.surname}"
                textHolder.textMessage.text = report.answer

                textHolder.textTp.typeface = type
                textHolder.textMessage.typeface = type
                textHolder.textName.typeface = typeBold

                if (report.pathFile != null && report.pathFile!!.isNotEmpty()){
                    holder.imageLayout.visibility = View.VISIBLE
                } else {
                    holder.imageLayout.visibility = View.GONE
                }

                if (userProfile!!.photo!!.isEmpty()){
                    Glide.with(holder.itemView.context).load(R.drawable.avatar_placeholder).into(holder.userImage)
                } else {
                    Glide.with(holder.itemView.context).load(userProfile!!.photo + "/performer?token=${loginService.accessToken}").into(textHolder.userImage)
                }


                holder.imageLayout.setOnClickListener {
                    callback?.invoke(report.pathFile!!)
                }
            }

        }
    }

    companion object {

        private val TEXT = 1
        private val MY_TEXT = 2
    }
}