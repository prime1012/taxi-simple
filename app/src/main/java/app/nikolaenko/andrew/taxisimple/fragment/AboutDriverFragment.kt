package app.nikolaenko.andrew.taxisimple.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import app.nikolaenko.andrew.taxisimple.R
import app.nikolaenko.andrew.taxisimple.model.DriverModel
import com.bumptech.glide.Glide
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.about_driver_fragment.*
import org.jetbrains.anko.support.v4.toast


class AboutDriverFragment : BaseFragment() {

    private var infoDisposable: Disposable? = null

    override fun onStart() {
        super.onStart()
        changeFont()

        getDriverInfo()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.about_driver_fragment, container, false)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }

    private fun changeFont(){
        changeFontInTextView(starCount)
        changeFontInTextView(phoneText)
        changeFontInTextView(emailText)
        changeFontInTextView(textDriverLicense)
        changeFontInTextView(textFrom)
        changeFontInTextView(textNumberCard)
        changeFontInTextView(text2)

        changeFontInTextViewBold(nameText)
        changeFontInTextViewBold(textNumber)
        changeFontInTextViewBold(textDate)
        changeFontInTextViewBold(textNumberLastCard)
    }

    private fun getDriverInfo(){

        showProgress(true)
        infoDisposable = taxiService.value.getDriverInfo(loginService.value.accessToken)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ t: DriverModel ->

                    nameText.text = "${t.name} ${t.middleName} \n${t.surname}"

                    if (t.photo != null) {

                        if (t.photo!!.isNotEmpty()) {
                            Glide.with(context!!).load(t.photo + "/performer?token=${loginService.value.accessToken}").into(userPhoto)
                        } else {
                            Glide.with(context!!).load(R.drawable.avatar_placeholder).into(userPhoto)
                        }
                    }

                    t.rating?.let { starCount.text = it }

                    val phoneNumb = t.phoneNumber
                    val result = StringBuilder(phoneNumb)
                            .insert(phoneNumb!!.length -11, "+")
                            .insert(phoneNumb.length - 9, " (")
                            .insert(phoneNumb.length - 4, ") ")
                            .insert(phoneNumb.length + 1, "-")
                            .insert(phoneNumb.length + 4, "-")
                            .toString()

                    phoneText.text = result
                    t.email?.let {  emailText.text = it }
                    textNumber.text = t.driverLicense!!.series.toString() + " " + t.driverLicense!!.number!!.toString()
                    textDate.text = t.driverLicense!!.dateOfExpiry.toString()

//                    textNumberCard.text = "5375****"
//                    textNumberLastCard.text = "2399"

                    if (t.bankСard!!.isNotEmpty()){
                        val sb = StringBuilder(t.bankСard)
                        sb.delete(4, 15)
                        sb.deleteCharAt(4)
                        val result2 = sb.toString()
                        textNumberCard.text = "$result2****"

                        val sb2 = StringBuilder(t.bankСard)
                        sb2.delete(0, 11)
                        sb2.deleteCharAt(0)
                        val result3 = sb2.toString()
                        textNumberLastCard.text = result3
                    } else {
                        textNumberCard.text = "пусто****"
                        textNumberLastCard.text = "пусто"
                    }






//                    textNumberCard.text = t.bankСard

                    showProgress(false)

                }, { e ->
                    e.printStackTrace()
                    toast(e.localizedMessage)
                    showProgress(false)
                })
    }


    override fun onStop() {
        super.onStop()
        infoDisposable?.dispose()
    }

}
