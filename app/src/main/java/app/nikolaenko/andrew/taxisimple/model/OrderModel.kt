package app.nikolaenko.andrew.taxisimple.model

import com.google.gson.annotations.SerializedName

class OrderModel {

    @SerializedName("order_id")
    var orderId: Int? = null

    @SerializedName("transportation_tariff_id")
    var transportationTariffId: Int? = null

    @SerializedName("service_ids")
    var serviceIds: String? = null

    @SerializedName("origin_latitude")
    var originLatitude: String? = null
    @SerializedName("origin_longitude")

    var originLongitude: String? = null
}
