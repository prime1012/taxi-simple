package app.nikolaenko.andrew.taxisimple

import android.app.Application
import android.content.ContextWrapper
import app.nikolaenko.andrew.taxisimple.network.LoginService
import app.nikolaenko.andrew.taxisimple.network.RetrofitFactory
import app.nikolaenko.andrew.taxisimple.network.TaxiPublicService
import app.nikolaenko.andrew.taxisimple.network.TaxiService
import com.github.salomonbrys.kodein.*
import com.microblink.MicroblinkSDK
import com.microblink.intent.IntentDataTransferMode
import com.pixplicity.easyprefs.library.Prefs
import org.jetbrains.anko.ctx

class App : Application(), KodeinAware {

    override val kodein by Kodein.lazy {
        bind<TaxiPublicService>() with provider { RetrofitFactory.getForLogin(ctx.getString(R.string.api_base_url)).create(TaxiPublicService::class.java) }
        bind<LoginService>() with provider { LoginService(ctx, kodein.instance()) }
        bind<TaxiService>() with provider { RetrofitFactory.getAuthorized(ctx.getString(R.string.api_base_url), kodein.instance(), applicationContext).create(TaxiService::class.java) }
    }

    override fun onCreate() {
        super.onCreate()

        Prefs.Builder()
                .setContext(this)
                .setMode(ContextWrapper.MODE_PRIVATE)
                .setPrefsName(packageName)
                .setUseDefaultSharedPreference(true)
                .build()

        MicroblinkSDK.setLicenseFile("MB_app.nikolaenko.andrew.taxisimple_Pdf417Mobi_Android_2019-01-06.mblic", this)
        MicroblinkSDK.setIntentDataTransferMode(IntentDataTransferMode.PERSISTED_OPTIMISED)

    }
}