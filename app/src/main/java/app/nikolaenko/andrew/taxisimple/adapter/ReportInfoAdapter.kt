package app.nikolaenko.andrew.taxisimple.adapter

import android.graphics.Typeface
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import app.nikolaenko.andrew.taxisimple.R
import app.nikolaenko.andrew.taxisimple.model.DriverModel
import app.nikolaenko.andrew.taxisimple.model.Report
import app.nikolaenko.andrew.taxisimple.model.SupportChatModel
import com.bumptech.glide.Glide
import org.jetbrains.anko.find
import java.util.*

class ReportInfoAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var list: MutableList<Report> = ArrayList()
    var myProfile: DriverModel? = null

    fun setList(list: List<Report>, myProfile: DriverModel) {
        this.list = list.toMutableList()
        this.myProfile = myProfile
        notifyDataSetChanged()
    }

    class TextMessageHolder constructor(view: View) : RecyclerView.ViewHolder(view) {
        internal var textTp: TextView = view.find(R.id.textType)
        internal var textName: TextView = view.find(R.id.textName)
        internal var textMessage: TextView = view.find(R.id.textMessage)
    }

    class MyTextMessageHolder constructor(view: View) : RecyclerView.ViewHolder(view) {
        internal var textTp: TextView = view.find(R.id.textType)
        internal var textName: TextView = view.find(R.id.textName)
        internal var textMessage: TextView = view.find(R.id.textMessage)
        internal var userImage: ImageView = view.find(R.id.userImage)
    }

    class NoCrashHolder constructor(view: View) : RecyclerView.ViewHolder(view)

    override fun getItemViewType(position: Int): Int {

        val message = list[position]

        return MY_TEXT

//        return if (message.answer != null) TEXT else MY_TEXT
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val mInflater = LayoutInflater.from(parent.context)
        when (viewType) {

            TEXT -> {
                val textGroup = mInflater.inflate(R.layout.support_user_item, parent, false) as ViewGroup
                return TextMessageHolder(textGroup)
            }

            MY_TEXT -> {
                val myTextGroup = mInflater.inflate(R.layout.support_my_item, parent, false) as ViewGroup
                return MyTextMessageHolder(myTextGroup)
            }

            else -> return NoCrashHolder(FrameLayout(parent.context))
        }
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val report = list[position]

        val type = Typeface.createFromAsset(holder.itemView.context!!.assets, "font/opensans_regular.ttf")
        val typeBold = Typeface.createFromAsset(holder.itemView.context!!.assets, "font/opensans_bold.ttf")

        when (holder.itemViewType) {

//            TEXT -> {
//                val textHolder = holder as TextMessageHolder
//                textHolder.textTp.text = "Диспетчер"
//                textHolder.textName.text = "Имя Фамилия"
//                textHolder.textMessage.text = report.answer!![0].answer
//
//                textHolder.textTp.typeface = type
//                textHolder.textMessage.typeface = type
//                textHolder.textName.typeface = typeBold
//            }

            MY_TEXT -> {
                val myTextHolder = holder as MyTextMessageHolder
                myTextHolder.textTp.text = "Пользователь"
                myTextHolder.textName.text = "${myProfile?.name} ${myProfile?.middleName} ${myProfile?.surname} "
                myTextHolder.textMessage.text = report.text

                Glide.with(holder.itemView.context).load(myProfile!!.photo).into(holder.userImage)

                myTextHolder.textTp.typeface = type
                myTextHolder.textMessage.typeface = type
                myTextHolder.textName.typeface = typeBold
            }
        }
    }

    companion object {

        private val TEXT = 1
        private val MY_TEXT = 2
    }
}