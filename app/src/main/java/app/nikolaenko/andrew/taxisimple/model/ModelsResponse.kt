package app.nikolaenko.andrew.taxisimple.model

import com.google.gson.annotations.SerializedName

import java.util.Date

class ModelsResponse<T> {

    @SerializedName("status")
    var isStatus: String? = null

    @SerializedName("data")
    var data: List<T>? = null
}
