package app.nikolaenko.andrew.taxisimple.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.DialogFragment
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v7.app.AppCompatActivity
import android.widget.FrameLayout
import android.widget.LinearLayout
import app.nikolaenko.andrew.taxisimple.R
import app.nikolaenko.andrew.taxisimple.fragment.DriverMainFragment
import app.nikolaenko.andrew.taxisimple.fragment.PhoneFragment
import app.nikolaenko.andrew.taxisimple.network.LoginService
import app.nikolaenko.andrew.taxisimple.network.TaxiPublicService
import app.nikolaenko.andrew.taxisimple.network.TaxiService
import com.github.salomonbrys.kodein.LazyKodein
import com.github.salomonbrys.kodein.android.KodeinAppCompatActivity
import com.github.salomonbrys.kodein.android.appKodein
import com.github.salomonbrys.kodein.instance
import com.pixplicity.easyprefs.library.Prefs
import org.jetbrains.anko.support.v4.toast
import org.jetbrains.anko.toast


class MainActivity : KodeinAppCompatActivity() {

    protected val kodein = LazyKodein(appKodein)
    protected val taxiService = kodein.instance<TaxiService>()
    protected val loginService = kodein.instance<LoginService>()

    protected val taxiPulicService = kodein.instance<TaxiPublicService>()

    var progressView: FrameLayout? = null
    var bottomTabs: LinearLayout? = null
    var fragment: Fragment? = null

    internal var doubleBackToExitPressedOnce = false

    @SuppressLint("MissingSuperCall")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)

        progressView = findViewById(R.id.progressMain)

        if (savedInstanceState == null) {

            if (loginService.value.isLoggedIn()){
                hardReplaceFragment(DriverMainFragment())
            } else {
                fragment = PhoneFragment()
                val ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.placeholder, fragment)
                ft.commit()
            }
        }
    }

    override fun onBackPressed() {

        if (supportFragmentManager.backStackEntryCount != 0) {
            super.onBackPressed()
            return
        }

        if (doubleBackToExitPressedOnce) {
            super.onBackPressed()
            finish()
            return
        }

        this.doubleBackToExitPressedOnce = true
        applicationContext.toast("Если хочешь выйти, нажми BACK еще раз")

        Handler().postDelayed({ doubleBackToExitPressedOnce = false }, 2000)
    }

    fun hardReplaceFragment(fragment: Fragment) {
        supportFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
        supportFragmentManager.beginTransaction()
                .replace(R.id.placeholder, fragment)
                .commit()
    }

    override fun onDestroy() {
        super.onDestroy()
        Prefs.remove("driver_info")
    }
}
