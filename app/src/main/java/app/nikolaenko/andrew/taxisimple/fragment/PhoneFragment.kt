package app.nikolaenko.andrew.taxisimple.fragment

import android.Manifest
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import app.nikolaenko.andrew.taxisimple.R
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.redmadrobot.inputmask.MaskedTextChangedListener
import kotlinx.android.synthetic.main.phone_fragment.*
import org.jetbrains.anko.support.v4.toast
import android.os.Environment.DIRECTORY_PICTURES
import android.view.inputmethod.InputMethodManager


class PhoneFragment : BaseFragment() {

    private var isAccepted: Boolean = false
    internal var phoneNumber: String? = null
    internal var phoneNumberFilled: Boolean = false

    private var imm: InputMethodManager? = null

    override fun onStart() {
        super.onStart()
        changeFont()
        showProgress(false)
        getPermissions()

        imm = context!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.phone_fragment, container, false)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btnContinue.setOnClickListener {
            if (isAccepted){

                if ( phoneNumberLogin.text.length  < 18 || phoneNumberLogin.text.isNullOrBlank()){
                    toast("Неправильный номер!")
                } else {
                    imm!!.showSoftInput(phoneNumberLogin, InputMethodManager.SHOW_IMPLICIT)
                    replaceFragment(HomeFragment.newInstance(phoneNumberLogin.text.toString().trim()), true)
                }

            } else {
                getPermissions()
                return@setOnClickListener
            }
        }

        val listener = MaskedTextChangedListener(
                "+7 ([000]) [000]-[00]-[00]",
                false,
                phoneNumberLogin,
                null,
                object : MaskedTextChangedListener.ValueListener {
                    override fun onTextChanged(maskFilled: Boolean, extractedValue: String) {
                        phoneNumber = extractedValue
                        phoneNumberFilled = maskFilled
                    }
                }
        )

        phoneNumberLogin.addTextChangedListener(listener)
        phoneNumberLogin.onFocusChangeListener = listener
        phoneNumberLogin.hint = listener.placeholder()
    }

    private fun getPermissions(){

        Dexter.withActivity(mainActivity)
                .withPermissions(
                        Manifest.permission.CAMERA, Manifest.permission.ACCESS_COARSE_LOCATION,  Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(object : MultiplePermissionsListener {

                    override fun onPermissionRationaleShouldBeShown(permissions: MutableList<com.karumi.dexter.listener.PermissionRequest>?, token: PermissionToken?) {
                        token?.continuePermissionRequest()
                    }

                    override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                        if (report?.areAllPermissionsGranted()!!){
                            isAccepted = true
                        }
                    }
                }).check()
    }

    private fun changeFont(){
        changeFontInTextView(text)
        changeFontInTextView(text2)
        changeFontInTextView(phoneNumberLogin)
        changeFontInTextView(textContinue)
    }
}
