package app.nikolaenko.andrew.taxisimple.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import app.nikolaenko.andrew.taxisimple.R
import kotlinx.android.synthetic.main.news_description_fragment.*


class NewsDescriptionFragment : BaseFragment() {

    var textFromWho: String? = null
    var textTime: String? = null
    var textDate: String? = null
    var textTtl: String? = null
    var textFull: String? = null

    override fun onStart() {
        super.onStart()
        changeFont()

        textFromWho = arguments?.getString(ARG_FROM_WHO)
        textTime = arguments?.getString(ARG_TIME)
        textDate = arguments?.getString(ARG_DATE)
        textTtl = arguments?.getString(ARG_TITLE)
        textFull = arguments?.getString(ARG_TEXT)

        title.text = textTtl
        txtFrom.text = textFromWho
        textOfNews.text = textFull
        date.text = textDate
        time.text = textTime
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.news_description_fragment, container, false)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        leftBtn.setOnClickListener { goBack() }

    }

    private fun changeFont(){
        changeFontInTextView(textTitle)
        changeFontInTextView(text1)
        changeFontInTextView(txtFrom)
        changeFontInTextView(textOfNews)
        changeFontInTextView(text2)
        changeFontInTextView(date)

        changeFontInTextViewBold(title)
        changeFontInTextViewBold(time)
    }

    companion object {

        val ARG_FROM_WHO = "arg_from_who"
        val ARG_TITLE = "arg_title"
        val ARG_TIME = "arg_time"
        val ARG_DATE = "arg_DATE"
        val ARG_TEXT = "arg_TEXT"

        fun newInstance(fromWho: String, time: String, date: String, title: String, text: String): NewsDescriptionFragment {

            val args = Bundle()

            args.putString(ARG_FROM_WHO, fromWho)
            args.putString(ARG_TIME, time)
            args.putString(ARG_DATE, date)
            args.putString(ARG_TITLE, title)
            args.putString(ARG_TEXT, text)

            val fragment = NewsDescriptionFragment()
            fragment.arguments = args
            return fragment
        }
    }
}
