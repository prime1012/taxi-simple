package app.nikolaenko.andrew.taxisimple.fragment

import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import app.nikolaenko.andrew.taxisimple.adapter.ActivityLinePagerAdapter
import app.nikolaenko.andrew.taxisimple.R
import app.nikolaenko.andrew.taxisimple.model.DriverModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.money_fragment.*
import org.jetbrains.anko.support.v4.toast
import org.jetbrains.anko.textColor


class MoneyFragment : BaseFragment() {

    var isForMap: Boolean = false
    private var infoDisposable: Disposable? = null

    override fun onStart() {
        super.onStart()
        changeFont()

        getDriverInfo()

        isForMap = arguments?.getBoolean(ARG_FOR_MAP)!!

        if (isForMap) {
            viewpager.currentItem = 1
        } else {
            viewpager.currentItem = 0
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.money_fragment, container, false)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupViewPager(viewpager)

        viewpager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener{
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }

            override fun onPageSelected(position: Int) {
                if (position == 0){
                    moneyImg.setImageResource(R.drawable.money_icon)
                    moneyText.textColor = ContextCompat.getColor(context!!, R.color.black)

                    autoImg.setImageResource(R.drawable.auto_not_active)
                    autoText.textColor = ContextCompat.getColor(context!!, R.color.main_gray)

                } else {
                    moneyImg.setImageResource(R.drawable.money_not_active)
                    moneyText.textColor = ContextCompat.getColor(context!!, R.color.main_gray)

                    autoImg.setImageResource(R.drawable.auto_icon)
                    autoText.textColor = ContextCompat.getColor(context!!, R.color.black)

                }
            }

        })

        leftBtn.setOnClickListener { goBack() }

        layoutTransactions.setOnClickListener {
            viewpager.currentItem = 0
        }

        layoutRides.setOnClickListener {
            viewpager.currentItem = 1
        }
    }

    private fun changeFont(){
        changeFontInTextView(balanceCurrency)
        changeFontInTextView(textTitle)

        changeFontInTextViewBold(balanceValue)
    }

    private fun setupViewPager(viewPager: ViewPager) {

        val adapter = ActivityLinePagerAdapter(childFragmentManager)
        adapter.addFragment(TransactionsFragment(), getString(R.string.transactions))
        adapter.addFragment(RidesFragment(), getString(R.string.rides))
        viewPager.adapter = adapter
    }


    private fun getDriverInfo(){

        infoDisposable = taxiService.value.getDriverInfo(loginService.value.accessToken)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ t: DriverModel ->

                    t.balance?.let { balanceValue.text = it.toString() }
                    balanceCurrency.text = "руб"

                }, { e ->
                    e.printStackTrace()
                    toast(e.localizedMessage)
                    showProgress(false)
                })

    }

    override fun onStop() {
        super.onStop()
        infoDisposable?.dispose()
    }

    companion object {

        val ARG_FOR_MAP = "arg_for_map"

        fun newInstance(isForMap: Boolean): MoneyFragment {

            val args = Bundle()

            args.putBoolean(ARG_FOR_MAP, isForMap)

            val fragment = MoneyFragment()
            fragment.arguments = args
            return fragment
        }
    }
}
