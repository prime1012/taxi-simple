package app.nikolaenko.andrew.taxisimple.adapter


import android.graphics.Typeface
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import app.nikolaenko.andrew.taxisimple.R
import app.nikolaenko.andrew.taxisimple.model.Image
import app.nikolaenko.andrew.taxisimple.model.NewsModel
import app.nikolaenko.andrew.taxisimple.model.RideModel
import com.bumptech.glide.Glide
import org.jetbrains.anko.find

class NewsAdapter(private val callback: ((fromWho: String?, time: String?, date: String?, title: String?, text: String?) -> Unit)? = null) : RecyclerView.Adapter<NewsAdapter.MyViewHolder>() {

    private var newsList: List<NewsModel> = listOf()

    fun setList(list: List<NewsModel>) {
        newsList = list
        notifyDataSetChanged()
    }

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var text: TextView = view.find(R.id.text)
        var textFrom: TextView = view.find(R.id.textFrom)
        var time: TextView = view.find(R.id.time)
        var date: TextView = view.find(R.id.date)
        var title: TextView = view.find(R.id.title)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.news_item, parent, false)

        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val newsItem = newsList[position]

        val type = Typeface.createFromAsset(holder.itemView.context?.assets, "font/opensans_regular.ttf")
        val typeBold = Typeface.createFromAsset(holder.itemView.context?.assets, "font/opensans_bold.ttf")

        holder.textFrom.text = newsItem.fromWho
        holder.time.text = newsItem.time
        holder.date.text = newsItem.date
        holder.title.text = newsItem.title

        holder.text.typeface = type
        holder.title.typeface = typeBold
        holder.textFrom.typeface = type
        holder.date.typeface = type
        holder.time.typeface = typeBold

        holder.itemView.setOnClickListener {
            callback?.invoke(newsItem.fromWho, newsItem.time, newsItem.date, newsItem.title, newsItem.text)
        }
    }

    override fun getItemCount(): Int {
        return newsList.size
    }
}
