package app.nikolaenko.andrew.taxisimple.Notification

import android.util.Log
import android.widget.Toast
import app.nikolaenko.andrew.taxisimple.network.LoginService
import app.nikolaenko.andrew.taxisimple.network.TaxiService
import com.github.salomonbrys.kodein.LazyKodein
import com.github.salomonbrys.kodein.android.appKodein
import com.github.salomonbrys.kodein.instance


import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.FirebaseInstanceIdService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

import org.json.JSONObject

class MyFirebaseInstanceIDService : FirebaseInstanceIdService() {

    override fun onTokenRefresh() {
        super.onTokenRefresh()
        val refreshedToken = FirebaseInstanceId.getInstance().token

        // Saving reg id to shared preferences
        //        Config.setFirebaseToken(this, refreshedToken);

        // sending reg id to your server
        sendRegistrationToServer(refreshedToken)

        // sending token to log
        Log.v("Firebase", refreshedToken)
    }

    private fun sendRegistrationToServer(token: String?) {

        val kodein = LazyKodein(appKodein)
        val yammyService = kodein.instance<TaxiService>()
        val loginService = kodein.instance<LoginService>()

        if (loginService.value.isLoggedIn()){

            yammyService.value.sendFirebaseToken(loginService.value.accessToken, token!!)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({ t ->
                        Log.e("fcm", "token send")
                    }, { e ->
                        e.printStackTrace()
                    })

        }
    }

    companion object {
        private val TAG = MyFirebaseInstanceIDService::class.java.simpleName
    }
}
